<!doctype html>
<html lang="en">

<head>

    @include('partials.frontsite.meta')

    <!-- Title -->
    <title>@yield('title') - Schoolmedia</title>

    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.png">

    @stack('before-style')

    <!-- style -->
    @include('partials.frontsite.style')

    @stack('after-style')

</head>

<body>

    <!-- Wrapper -->
    <div id="wrapper">
 
        @include('sweetalert::alert')
        @include('partials.frontsite.sidebar')
        @include('partials.frontsite.header')
            @yield('content')
        @include('partials.frontsite.chat')

    </div>

</body>

    @stack('before-script')
    
    <!-- script -->
    @include('partials.frontsite.script')
    
    @stack('after-script')

</html>