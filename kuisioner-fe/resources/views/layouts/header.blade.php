<!doctype html>
<html lang="en">

<head>

    <!-- Title -->
    <title>Sosmed - Schoolmedia</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Connecting People with School Communities">

    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/images/favicon.png')}">

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}">
    <link rel="stylesheet" href="{{asset('assets/css/night-mode.css')}">
    <link rel="stylesheet" href="{{asset('assets/css/framework.css')}">

    <!-- Icon -->
    <link rel="stylesheet" href="{{asset/css/icons.css')}">

</head>

<body>

    <!-- Wrapper -->
    <div id="wrapper">