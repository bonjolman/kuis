@yield('header')

@yield('head')

@yield('side')

@yield('content')

@yield('overlay')

@yield('footer')