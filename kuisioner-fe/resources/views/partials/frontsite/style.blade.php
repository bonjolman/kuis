<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/frontsite/template/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/frontsite/template/css/night-mode.css') }}">
<link rel="stylesheet" href="{{ asset('assets/frontsite/template/css/framework.css') }}">

<!-- Icon -->
<link rel="stylesheet" href="{{ asset('assets/frontsite/template/css/icons.css') }}">