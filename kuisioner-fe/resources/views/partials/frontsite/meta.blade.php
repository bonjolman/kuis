<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Connecting People with School Communities">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">