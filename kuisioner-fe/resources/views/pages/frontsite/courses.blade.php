@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'course')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Courses </h1>
            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-1 mb-5">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggestions </a></li>
                        <li><a href="#"> Newest </a></li>
                        <li><a href="#"> My videos </a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle uk-visible@s"> 
                    <i class="uil-plus"></i> Create new
                </a>
            </div>

            <!-- Videos sliders 1 -->
            <div class="uk-position-relative mb-2" uk-slider="finite: true">
                <div class="uk-slider-container video-grid-slider pb-3">
                    <ul class="uk-slider-items uk-child-width-1-4@m uk-child-width-1-3@s  pr-lg-1 uk-grid" uk-scrollspy="target: > div; cls: uk-animation-slide-bottom-small; delay: 100">
                        <li>
                            <a href="course-intro.html">
                                <div class="course-card">
                                    <div class="course-card-thumbnail ">
                                        <img src="assets/images/course/2.png">
                                        <span class="play-button-trigger"></span>
                                    </div>
                                    <div class="course-card-body">
                                        <div class="course-card-info">
                                            <div>
                                                <span class="catagroy">Angular</span>
                                            </div>
                                            <div>
                                                <i class="icon-feather-bookmark icon-small"></i>
                                            </div>
                                        </div>
                                        <h4>Learn Angular Fundamentals </h4>
                                        <p> Learn how to build launch React web applications using .. </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="course-intro.html">
                                <div class="course-card">
                                    <div class="course-card-thumbnail ">
                                        <img src="assets/images/course/3.png">
                                        <span class="play-button-trigger"></span>
                                    </div>
                                    <div class="course-card-body">
                                        <div class="course-card-info">
                                            <div>
                                                <span class="catagroy">JavaScript</span>
                                            </div>
                                            <div>
                                                <i class="icon-feather-bookmark icon-small"></i>
                                            </div>
                                        </div>
                                        <h4>The Complete JavaScript </h4>
                                        <p> JavaScript is how you build interactivity on the web.. </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="course-intro.html">
                                <div class="course-card">
                                    <div class="course-card-thumbnail">
                                        <img src="assets/images/course/1.png">
                                        <span class="play-button-trigger"></span>
                                    </div>
                                    <div class="course-card-body">
                                        <div class="course-card-info">
                                            <div>
                                                <span class="catagroy">HTML</span>
                                            </div>
                                            <div>
                                                <i class="icon-feather-bookmark icon-small"></i>
                                            </div>
                                        </div>

                                        <h4>Ultimate Web Developer Course </h4>
                                        <p> HTML is the building blocks of the web. It gives pages structure</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="course-intro.html">
                                <div class="course-card">
                                    <div class="course-card-thumbnail">
                                        <img src="assets/images/course/5.png">
                                        <span class="play-button-trigger"></span>
                                    </div>
                                    <div class="course-card-body">
                                        <div class="course-card-info">
                                            <div>
                                                <span class="catagroy">HTML</span>
                                            </div>
                                            <div>
                                                <i class="icon-feather-bookmark icon-small"></i>
                                            </div>
                                        </div>

                                        <h4>Ultimate Web Developer Course </h4>
                                        <p> HTML is the building blocks of the web. It gives pages structure</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="course-intro.html">
                                <div class="course-card">
                                    <div class="course-card-thumbnail ">
                                        <img src="assets/images/course/2.png">
                                        <span class="play-button-trigger"></span>
                                    </div>
                                    <div class="course-card-body">
                                        <div class="course-card-info">
                                            <div>
                                                <span class="catagroy">Angular</span>
                                            </div>
                                            <div>
                                                <i class="icon-feather-bookmark icon-small"></i>
                                            </div>
                                        </div>
                                        <h4>Learn Angular Fundamentals </h4>
                                        <p> Learn how to build launch React web applications using .. </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
            </div>

            <div class="section-header pb-0">
                <div class="section-header-left">
                    <h3> Categories </h3>
                    <p> Find a Course <span class="uk-visible@s"> by browsing top Categories </span></p>
                </div>
                <div class="section-header-right">
                    <a href="#" class="see-all"> See all</a>
                </div>
            </div>

            <div class="uk-position-relative mb-4 mt-4" uk-slider="finite: true">
                <div class="uk-slider-container py-3 px-1">
                    <ul class="uk-slider-items uk-child-width-1-6@m uk-child-width-1-3@s uk-child-width-1-2 uk-grid-small uk-grid">
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-3.png" alt="">
                                    <h4> Angular </h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-1.png" alt="">
                                    <h4> CSS </h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-4.png" alt="">
                                    <h4> Python </h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-5.png" alt="">
                                    <h4> Swift </h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-2.png" alt="">
                                    <h4> React </h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-6.png" alt="">
                                    <h4> WordPress </h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="course-card-category">
                                    <img src="assets/images/course/img-3.png" alt="">
                                    <h4> Angular </h4>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <h3 class="mb-2"> Web Development Courses </h3>
            <nav class="responsive-tab style-4 mb-5">
                <ul>
                    <li class="uk-active"><a href="#"> Suggestions </a></li>
                    <li><a href="#"> Newest </a></li>
                    <li><a href="#"> Mostly </a></li>
                </ul>
            </nav>

            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-3-4@m">
                    <div class="course-card course-card-list">
                        <div class="course-card-thumbnail">
                            <img src="assets/images/course/1.png">
                            <a href="course-intro.html" class="play-button-trigger"></a>
                        </div>
                        <div class="course-card-body">
                            <a href="course-intro.html">
                                <h4>Ultimate Web Designer And Developer Course </h4>
                            </a>
                            <p>HTML is the building blocks of the web. It gives pages structure and applies meaning
                                to content. Take this course to learn 
                            </p>
                            <div class="course-details-info">
                                <ul>
                                    <li> <i class="icon-feather-sliders"></i> Advance level </li>
                                    <li> By <a href="user-profile-1.html">Jonathan Madano </a> </li>
                                    <li class="uk-visible@s">
                                        <div class="star-rating">
                                            <span class="avg"> 4.8 </span> 
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="course-card course-card-list">
                        <div class="course-card-thumbnail">
                            <img src="assets/images/course/2.png">
                            <a href="course-intro.html" class="play-button-trigger"></a>
                        </div>
                        <div class="course-card-body">
                            <a href="course-intro.html">
                                <h4> Learn Angular Fundamentals From The Beginning </h4>
                            </a>
                            <p>Kick your Angular skills into action with this comprehensive dive into Angular and
                                getting started the right way.
                            </p>
                            <div class="course-details-info">
                                <ul>
                                    <li> <i class="icon-feather-sliders"></i> Binginner level </li>
                                    <li> By <a href="user-profile-1.html"> Stella Johnson</a> </li>
                                    <li class="uk-visible@s">
                                        <div class="star-rating">
                                            <span class="avg"> 4.4 </span> 
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star half"></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="course-card course-card-list">
                        <div class="course-card-thumbnail">
                            <img src="assets/images/course/3.png">
                            <a href="course-intro.html" class="play-button-trigger"></a>
                        </div>
                        <div class="course-card-body">
                            <a href="course-intro.html">
                                <h4> The Complete JavaScript From Scratch To Experts</h4>
                            </a>
                            <p>JavaScript is how you build interactivity on the web. Take this course to learn how
                                to work
                                with the DOM, create Objects... 
                            </p>
                            <div class="course-details-info">
                                <ul>
                                    <li> <i class="icon-feather-sliders"></i> Advance level </li>
                                    <li> By <a href="user-profile-1.html"> Adrian Mohani </a> </li>
                                    <li class="uk-visible@s">
                                        <div class="star-rating">
                                            <span class="avg"> 4.9 </span> 
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star half"></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="course-card course-card-list">
                        <div class="course-card-thumbnail">
                            <img src="assets/images/course/7.png">
                            <a href="course-intro.html" class="play-button-trigger"></a>
                        </div>
                        <div class="course-card-body">
                            <a href="course-intro.html">
                                <h4> Bootstrap 4 From Scratch With 5 Real Projects </h4>
                            </a>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt
                                ut labore et dolore magna aliqua...
                            </p>
                            <div class="course-details-info">
                                <ul>
                                    <li> <i class="icon-feather-sliders"></i> Binginner level </li>
                                    <li> By <a href="user-profile-1.html">Jonathan Madano </a> </li>
                                    <li class="uk-visible@s">
                                        <div class="star-rating">
                                            <span class="avg"> 4.4 </span> 
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star half"></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="course-card course-card-list">
                        <div class="course-card-thumbnail">
                            <img src="assets/images/course/6.png">
                            <a href="course-intro.html" class="play-button-trigger"></a>
                        </div>
                        <div class="course-card-body">
                            <a href="course-intro.html">
                                <h4> The Complete 2020 Web Development Bootcamp</h4>
                            </a>
                            <p> HTML is the building blocks of the web. It gives pages structure and applies meaning
                                to
                                content. Take this course to learn
                            </p>
                            <div class="course-details-info">
                                <ul>
                                    <li> <i class="icon-feather-sliders"></i> Binginner level </li>
                                    <li> By <a href="#"> Stella Johnson </a> </li>
                                    <li class="uk-visible@s">
                                        <div class="star-rating">
                                            <span class="avg"> 4.9 </span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star half"></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- pagination menu -->
                    <ul class="uk-pagination my-5 uk-flex-center" uk-margin>
                        <li>
                            <a href="#"><span uk-pagination-previous></span></a>
                        </li>
                        <li class="uk-disabled">
                            <span>...</span>
                        </li>
                        <li>
                            <a href="#">4</a>
                        </li>
                        <li class="uk-active">
                            <span>7</span>
                        </li>
                        <li>
                            <a href="#">8</a>
                        </li>
                        <li>
                            <a href="#">10</a>
                        </li>
                        <li class="uk-disabled">
                            <span>...</span>
                        </li>
                        <li>
                            <a href="#"><span uk-pagination-next></span></a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand">
                    <div class="sidebar-filter" uk-sticky="offset:30 ; media : @s: bottom: true">
                        <div class="sidebar-filter-contents">
                            <h4> Filter</h4>
                            <ul class="sidebar-filter-list uk-accordion" uk-accordion="multiple: true">
                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Skill Levels </a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <div class="uk-form-controls">
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio1" checked>
                                                <span class="test"> Beginner <span> (25) </span> </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio1">
                                                <span class="test"> Entermidate<span> (32) </span></span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio1">
                                                <span class="test"> Expert <span> (12) </span></span>
                                            </label>
                                        </div>
                                    </div>
                                </li>

                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Course type </a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <div class="uk-form-controls">
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2" checked>
                                                <span class="test"> Free (42) </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2">
                                                <span class="test"> Paid </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>

                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Duration time </a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <div class="uk-form-controls">
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3" checked>
                                                <span class="test"> +5 Hourse (23) </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3">
                                                <span class="test"> +10 Hourse (12)</span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3">
                                                <span class="test"> +20 Hourse (5)</span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3">
                                                <span class="test"> +30 Hourse (2)</span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush