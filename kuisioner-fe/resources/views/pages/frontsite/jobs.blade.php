@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Jobs </h1>

            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-2 mb-4">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggestions </a></li>
                        <li><a href="#"> Newest </a></li>
                        <li><a href="#"> My Jobs</a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle uk-visible@s"> 
                    <i class="uil-plus"></i>
                    Create new
                </a>
            </div>

            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-2-3@m">
                    <!-- job 1 -->
                    <a href="#" class="job-block">
                        <div class="job-block-details">
                            <div class="job-block-logo">
                                <img src="assets/images/brand/brand-avatar-4.png" alt="">
                            </div>

                            <div class="job-block-description">
                                <h4>Phaseout </h4>
                                <h3> Technical Event Support Specialist </h3>
                                <p class="job-block-text"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                                    wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper ..
                                </p>
                                <div class="job-block-tags">
                                    <button class="button"> Wordpress </button>
                                    <button class="button"> Design </button>
                                    <button class="button"> Joomla </button>
                                </div>
                            </div>
                        </div>
                    </a>

                    <!-- job 2-->
                    <a href="#" class="job-block">
                        <div class="job-block-details">
                            <div class="job-block-logo">
                                <img src="assets/images/brand/brand-avatar-3.png" alt="">
                            </div>

                            <div class="job-block-description">
                                <h4> Suranna </h4>
                                <h3>Sky Accounting and mobile apps</h3>
                                <p class="job-block-text"> Ut wisi enim ad minim veniam, quis nostrud exerci tation
                                    ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                </p>
                                <div class="job-block-tags">
                                    <button class="button"> Java </button>
                                    <button class="button"> Design </button>
                                    <button class="button"> Typescript </button>
                                </div>
                            </div>
                        </div>
                    </a>

                    <!-- job 3 -->
                    <a href="#" class="job-block">
                        <div class="job-block-details">
                            <div class="job-block-logo">
                                <img src="assets/images/brand/brand-avatar-1.png" alt="">
                            </div>

                            <div class="job-block-description">
                                <h4> Instarred </h4>
                                <h3> IT Department Manager & Blogger-Entrepenour </h3>
                                <p class="job-block-text"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                                    wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper ..
                                </p>
                                <div class="job-block-tags">
                                    <button class="button"> Wordpress </button>
                                    <button class="button"> Design </button>
                                    <button class="button"> Joomla </button>
                                </div>
                            </div>
                        </div>
                    </a>

                    <!-- job 4-->
                    <a href="#" class="job-block">
                        <div class="job-block-details">
                            <div class="job-block-logo">
                                <img src="assets/images/brand/brand-avatar-2.png" alt="">
                            </div>

                            <div class="job-block-description">
                                <h4>Reveal</h4>
                                <h3> Hardware Verification & Validation Engineer </h3>
                                <p class="job-block-text">Phasellus enim magna, varius et commodo ut, ultricies
                                    vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel
                                    justo. In libero urna, venenatis sit amet ornare non
                                </p>
                                <div class="job-block-tags">
                                    <button class="button"> Architect </button>
                                    <button class="button"> Design </button>
                                    <button class="button"> Engineering </button>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- sidebar -->
                <div class="uk-width-expand">
                    <div class="sidebar-filter" uk-sticky="offset:30 ; media : @s: bottom: true">
                        <div class="sidebar-filter-contents">
                            <h3> Filter</h3>

                            <ul class="sidebar-filter-list uk-accordion" uk-accordion="multiple: true">
                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Skill Levels </a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <div class="uk-form-controls">
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio1" checked>
                                                <span class="test"> Beginner <span> (25) </span> </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio1">
                                                <span class="test"> Entermidate<span> (32) </span></span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio1">
                                                <span class="test"> Expert <span> (12) </span></span>
                                            </label>
                                        </div>
                                    </div>
                                </li>

                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Job Type </a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <div class="uk-form-controls">
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2">
                                                <span class="test"> Freelance (42) </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2" checked>
                                                <span class="test"> Full Time </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2">
                                                <span class="test"> Part Time </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2">
                                                <span class="test"> Temporary </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio2">
                                                <span class="test"> Full Time </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>

                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Duration time </a>
                                    <div class="uk-accordion-content" aria-hidden="false">
                                        <div class="uk-form-controls">
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3" checked>
                                                <span class="test"> +5 Hourse (23) </span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3">
                                                <span class="test"> +10 Hourse (12)</span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3">
                                                <span class="test"> +20 Hourse (5)</span>
                                            </label>
                                            <label>
                                                <input class="uk-radio" type="radio" name="radio3">
                                                <span class="test"> +30 Hourse (2)</span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <input type="text" class="uk-input bg-grey border-0" placeholder="Search" name="" id="">
                    <ul class="uk-list menu-list">
                        <li class="list-title"><a href="#"> Categories </a></li>
                        <li><a href="#"> Data Analytics </a></li>
                        <li><a href="#"> Design  Creative </a></li>
                        <li><a href="#"> Data Analytics </a></li>
                        <li><a href="#"> Sales Marketing </a></li>
                        <li><a href="#"> Customer Service </a></li>
                        <li><a href="#"> Video editing </a></li>
                        <li><a href="#"> Prototypes </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush