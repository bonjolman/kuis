@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Gaming </h1>
            <nav class="responsive-tab style-1 mb-5">
                <ul>
                    <li class="uk-active"><a href="#"> Newest </a></li>
                    <li><a href="#"> Suggestions  </a></li>
                    <li><a href="#"> Popular  </a></li>
                </ul>
            </nav>

            <div class="uk-position-relative" uk-slider="finite: true">
                <div class="uk-slider-container pb-3">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-grid-small uk-grid" uk-scrollspy="target: > div; cls: uk-animation-slide-bottom-small; delay: 100">
                        <li>
                            <div class="game-card">
                                <!-- Group Card Thumbnail -->
                                <div class="game-card-thumbnail">
                                    <img src="assets/images/games/img-1.jpg" alt="">
                                </div>
                                <!-- Group Card Content -->
                                <div class="game-card-content">
                                    <h3> PUBG Mobile </h3>
                                    <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                    <p class="info"> 
                                        <a href="#"><span> 232k are playing </span></a>
                                    </p>
                                    <div class="game-card-btns">
                                        <a href="games-single.html" class="button btn-default small"> Play now </a>
                                    </div>
                                </div>
                            </div>
                        </li> 
                        <li>
                            <div class="game-card">
                                <!-- Group Card Thumbnail -->
                                <div class="game-card-thumbnail">
                                    <img src="assets/images/games/img-2.jpg" alt="">
                                </div>
                                <!-- Group Card Content -->
                                <div class="game-card-content">
                                    <h3> ChooxTv </h3>
                                    <p class="mb-1 uk-text-small"> MARVEL Super War  Mobile Legends </p>
                                    <p class="info"> 
                                        <a href="#"><span> 232k are playing </span></a>
                                    </p>
                                    <div class="game-card-btns">
                                        <a href="games-single.html" class="button btn-default small"> Play now </a>
                                    </div>
                                </div>
                            </div>
                        </li> 
                        <li>
                            <div class="game-card">
                                <!-- Group Card Thumbnail -->
                                <div class="game-card-thumbnail">
                                    <img src="assets/images/games/img-3.jpg" alt="">
                                </div>
                                <!-- Group Card Content -->
                                <div class="game-card-content">
                                    <h3> Larion TV </h3>
                                    <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                    <p class="info"> 
                                        <a href="#"><span> 232k are playing </span></a>
                                    </p>
                                    <div class="game-card-btns">
                                        <a href="games-single.html" class="button btn-default small"> Play now </a>
                                    </div>
                                </div>
                            </div>
                        </li> 
                        <li>
                            <div class="game-card">
                                <!-- Group Card Thumbnail -->
                                <div class="game-card-thumbnail">
                                    <img src="assets/images/games/img-4.jpg" alt="">
                                </div>
                                <!-- Group Card Content -->
                                <div class="game-card-content">
                                    <h3> Mobile Legends </h3>
                                    <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                    <p class="info"> 
                                        <a href="#"><span> 232k are playing </span></a>
                                    </p>
                                    <div class="game-card-btns">
                                        <a href="games-single.html" class="button btn-default small"> Play now </a>
                                    </div>
                                </div>
                            </div>
                        </li> 
                        <li>
                            <div class="game-card">
                                <!-- Group Card Thumbnail -->
                                <div class="game-card-thumbnail">
                                    <img src="assets/images/games/img-5.jpg" alt="">
                                </div>
                                <!-- Group Card Content -->
                                <div class="game-card-content">
                                    <h3> Minecraft </h3>
                                    <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                    <p class="info"> 
                                        <a href="#"><span> 232k are playing </span></a>
                                    </p>
                                    <div class="game-card-btns">
                                        <a href="games-single.html" class="button btn-default small"> Play now </a>
                                    </div>
                                </div>
                            </div>
                        </li> 
                        <li>
                            <div class="game-card">
                                <!-- Group Card Thumbnail -->
                                <div class="game-card-thumbnail">
                                    <img src="assets/images/games/img-1.jpg" alt="">
                                </div>
                                <!-- Group Card Content -->
                                <div class="game-card-content">
                                    <h3> PUBG Mobile </h3>
                                    <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                    <p class="info"> 
                                        <a href="#"><span> 232k are playing </span></a>
                                    </p>
                                    <div class="game-card-btns">
                                        <a href="games-single.html" class="button btn-default small"> Play now </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <div class="section-small">
                <h2> Recently live streamers </h2>
                <div class="uk-position-relative" uk-slider="finite: true">
                    <div class="uk-slider-container pb-3">
                        <ul class="uk-slider-items uk-child-width-1-3@m uk-child-width-1-2@s uk-grid-match uk-grid" uk-scrollspy="target: > div; cls: uk-animation-slide-bottom-small; delay: 100">
                            <li>
                                <img src="assets/images/games/img-lg-1.jpg" class="rounded" alt="">
                                <div class="uk-flex-middle uk-grid-small mt-2" uk-grid>
                                    <div class="uk-width-auto"> 
                                        <img src="assets/images/avatars/avatar-2.jpg" class="uk-border-circle" width="40" height="40" alt="">
                                    </div>
                                    <div class="uk-width-expand pl-2"> 
                                        <h5 class="mb-0"> Strike Force Heroes 2 </h5>
                                        <p class="uk-text-small"> Young gamer · PUBG Mobile</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <img src="assets/images/games/img-lg-2.jpg" class="rounded" alt="">
                                <div class="uk-flex-middle uk-grid-small mt-2" uk-grid>
                                    <div class="uk-width-auto"> 
                                        <img src="assets/images/avatars/avatar-3.jpg" class="uk-border-circle" width="40" height="40" alt="">
                                    </div>
                                    <div class="uk-width-expand pl-2"> 
                                        <h5 class="mb-0"> Free Fire - Battlegrounds </h5>
                                        <p class="uk-text-small"> Smart Player  · PUBG Mobile</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <img src="assets/images/games/img-lg-3.jpg" class="rounded" alt="">
                                <div class="uk-flex-middle uk-grid-small mt-2" uk-grid>
                                    <div class="uk-width-auto"> 
                                        <img src="assets/images/avatars/avatar-5.jpg" class="uk-border-circle" width="40" height="40" alt="">
                                    </div>
                                    <div class="uk-width-expand pl-2"> 
                                        <h5 class="mb-0"> Clip of Wolv Gamers playing   </h5>
                                        <p class="uk-text-small"> Official gamer  · PUBG Mobile</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <img src="assets/images/games/img-lg-1.jpg" class="rounded" alt="">
                                <div class="uk-flex-middle uk-grid-small mt-2" uk-grid>
                                    <div class="uk-width-auto"> 
                                        <img src="assets/images/avatars/avatar-2.jpg" class="uk-border-circle" width="40" height="40" alt="">
                                    </div>
                                    <div class="uk-width-expand pl-2"> 
                                        <h5 class="mb-0"> Strike Force Heroes 2 </h5>
                                        <p class="uk-text-small"> Young gamer · PUBG Mobile</p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                        <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                    </div>
                </div>
            </div>

            <hr class="my-3 my-sm-2">

            <div class="section-small">
                <div class="uk-position-relative" uk-slider="finite: true">
                    <div class="grid-slider-header">
                        <div>
                            <h3> Brand Collection </h3>
                        </div>
                        <div class="grid-slider-header-link">
                            <a href="#" class="button btn-white uk-visible@m"> View all </a>
                            <a href="#" class="slide-nav-prev uk-invisible" uk-slider-item="previous"></a>
                            <a href="#" class="slide-nav-next" uk-slider-item="next"></a>
                        </div>
                    </div>

                    <div class="uk-slider-container pb-3">
                        <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-grid-small uk-grid" uk-scrollspy="target: > div; cls: uk-animation-slide-bottom-small; delay: 100">
                            <li>
                                <div class="game-card">
                                    <!-- Group Card Thumbnail -->
                                    <div class="game-card-thumbnail">
                                        <img src="assets/images/games/img-1.jpg" alt="">
                                    </div>
                                    <!-- Group Card Content -->
                                    <div class="game-card-content">
                                        <h3> PUBG Mobile </h3>
                                        <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                        <p class="info"> 
                                            <a href="#"><span> 232k are playing </span></a>
                                        </p>
                                        <div class="game-card-btns">
                                            <a href="games-single.html" class="button btn-default small"> Play now </a>
                                        </div>
                                    </div>
                                </div>
                            </li> 
                            <li>
                                <div class="game-card">
                                    <!-- Group Card Thumbnail -->
                                    <div class="game-card-thumbnail">
                                        <img src="assets/images/games/img-2.jpg" alt="">
                                    </div>
                                    <!-- Group Card Content -->
                                    <div class="game-card-content">
                                        <h3> ChooxTv </h3>
                                        <p class="mb-1 uk-text-small"> MARVEL Super War  Mobile Legends   </p>
                                        <p class="info"> 
                                            <a href="#"><span> 232k are playing </span></a>
                                        </p>
                                        <div class="game-card-btns">
                                            <a href="games-single.html" class="button btn-default small"> Play now </a>
                                        </div>
                                    </div>
                                </div>
                            </li> 
                            <li>
                                <div class="game-card">
                                    <!-- Group Card Thumbnail -->
                                    <div class="game-card-thumbnail">
                                        <img src="assets/images/games/img-3.jpg" alt="">
                                    </div>
                                    <!-- Group Card Content -->
                                    <div class="game-card-content">
                                        <h3>  Larion TV </h3>
                                        <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                        <p class="info"> 
                                            <a href="#"><span> 232k are playing </span></a>
                                        </p>
                                        <div class="game-card-btns">
                                            <a href="games-single.html" class="button btn-default small"> Play now </a>
                                        </div>
                                    </div>
                                </div>
                            </li> 
                            <li>
                                <div class="game-card">
                                    <!-- Group Card Thumbnail -->
                                    <div class="game-card-thumbnail">
                                        <img src="assets/images/games/img-4.jpg" alt="">
                                    </div>
                                    <!-- Group Card Content -->
                                    <div class="game-card-content">
                                        <h3> Mobile Legends </h3>
                                        <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                        <p class="info"> 
                                            <a href="#"><span> 232k are playing </span></a>
                                        </p>
                                        <div class="game-card-btns">
                                            <a href="games-single.html" class="button btn-default small"> Play now </a>
                                        </div>
                                    </div>
                                </div>
                            </li> 
                            <li>
                                <div class="game-card">
                                    <!-- Group Card Thumbnail -->
                                    <div class="game-card-thumbnail">
                                        <img src="assets/images/games/img-5.jpg" alt="">
                                    </div>
                                    <!-- Group Card Content -->
                                    <div class="game-card-content">
                                        <h3> Minecraft </h3>
                                        <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                        <p class="info"> 
                                            <a href="#"><span> 232k are playing </span></a>
                                        </p>
                                        <div class="game-card-btns">
                                            <a href="games-single.html" class="button btn-default small"> Play now </a>
                                        </div>
                                    </div>
                                </div>
                            </li> 
                            <li>
                                <div class="game-card">
                                    <!-- Group Card Thumbnail -->
                                    <div class="game-card-thumbnail">
                                        <img src="assets/images/games/img-1.jpg" alt="">
                                    </div>
                                    <!-- Group Card Content -->
                                    <div class="game-card-content">
                                        <h3> PUBG Mobile </h3>
                                        <p class="mb-1 uk-text-small"> Live now playing Mobile Legends </p>
                                        <p class="info"> 
                                            <a href="#"><span> 232k are playing </span></a>
                                        </p>
                                        <div class="game-card-btns">
                                            <a href="games-single.html" class="button btn-default small"> Play now </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush