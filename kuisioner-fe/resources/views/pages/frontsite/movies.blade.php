@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Movies </h1>
            <nav class="responsive-tab style-1 mb-5">
                <ul>
                    <li class="uk-active"><a href="#"> Newest </a></li>
                    <li><a href="#"> Suggestions  </a></li>
                    <li><a href="#"> Popular  </a></li>
                </ul>
            </nav>

            <div class="uk-position-relative" uk-slider="finite: true">
                <div class="uk-slider-container pb-3">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2 uk-grid uk-grid-small">
                        <li> 
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-3.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Skyfall </h2>
                                        <h6> United Kindom </h6>
    
                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>
    
                                        <div class="mov-card-tags">
                                            <span class="fantasy">Fantasy</span>
                                            <span class="scifi">Sci Fi</span>
                                        </div>
    
                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>
    
                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li> 
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-4.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Code8 </h2>
                                        <h6> Juiced By Jimmy Heuang </h6>
    
                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>
    
                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>
    
                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>
    
                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li> 
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>
    
                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>
    
                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>
    
                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>
    
                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li> 
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-2.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2> Mulan </h2>
                                        <h6>Juiced By Jimmy Heuang </h6>
    
                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>
    
                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>
    
                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>
    
                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li> 
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-5.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Sefirin Kizi</h2>
                                        <h6>Arab </h6>
    
                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>
    
                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>
    
                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>
    
                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li> 
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-3.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2> Skyfall </h2>
                                        <h6> United Kindom </h6>
    
                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>
    
                                        <div class="mov-card-tags">
                                            <span class="fantasy">Fantasy</span>
                                            <span class="scifi">Sci Fi</span>
                                        </div>
    
                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>
    
                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <hr class="my-3 my-sm-2">

            <div class="section-header mt-2">
                <div class="section-header-left">
                    <h3> Dramma Movies </h3>
                    <p> Consectetur adipisic ing elit </p>
                </div>
                <div class="section-header-right">
                    <a href="#" class="see-all"> See all</a>
                </div>
            </div>

            <div class="section-small pt-0">
                <div uk-slider="finite: true">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2 uk-grid uk-grid-small">
                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-2.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2> Mulan </h2>
                                        <h6>Juiced By Jimmy Heuang </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-5.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Sefirin Kizi</h2>
                                        <h6>Arab </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-3.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Skyfall </h2>
                                        <h6> United Kindom </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Fantasy</span>
                                            <span class="scifi">Sci Fi</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-4.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Code8 </h2>
                                        <h6> Juiced By Jimmy Heuang </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <div class="section-header">
                <div class="section-header-left">
                    <h3> Action Movies </h3>
                    <p> Consectetur adipisic ing elit </p>
                </div>
                <div class="section-header-right">
                    <a href="#" class="see-all"> See all</a>
                </div>
            </div>

            <div class="section-small pt-0">
                <div uk-slider="finite: true">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2 uk-grid uk-grid-small mb-3">
                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-2.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2> Mulan </h2>
                                        <h6>Juiced By Jimmy Heuang </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-5.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Sefirin Kizi</h2>
                                        <h6>Arab </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-3.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Skyfall </h2>
                                        <h6> United Kindom </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Fantasy</span>
                                            <span class="scifi">Sci Fi</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-4.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Code8 </h2>
                                        <h6> Juiced By Jimmy Heuang </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per family</p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named Frank </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush