@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    {{-- Content --}}
    <div class="main_content">
        <div class="main_content_inner pt-0">
            <div class="profile">
                <div class="profile-cover">
                    <!-- profile cover -->
                    <img src="assets/images/avatars/profile-cover.jpg" alt="">
                    <a href="#"> <i class="uil-camera"></i> Edit </a>
                </div>

                <div class="profile-details">
                    <div class="profile-image">
                        <img src="assets/images/avatars/avatar-2.jpg" alt="">
                        <a href="#"> </a>
                    </div>
                    <div class="profile-details-info">
                        <h1> Josephine Williams </h1>
                        <p> Family , Food , Fashion , Fourever <a href="#">Edit </a></p>
                    </div>
                </div>

                <div class="nav-profile" uk-sticky="offset:61;media : @s">
                    <div class="py-md-2 uk-flex-last">
                        <a href="#" class="button btn-schoolmedia mr-2"> <i class="uil-user-plus"></i> Add friend</a>
                        <a href="#" class="button btn-default mr-2"> <i class="uil-envelope"> </i> Send Message </i></a>
                        <a href="#" class="button btn-default button-icon"> <i class="uil-ellipsis-h"> </i></a>
                        <div uk-dropdown="pos: bottom-left ; mode:hover ">
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="#"> View as guast </a></li>
                                <li><a href="#"> Bloc this person </a></li>
                                <li><a href="#"> Report abuse</a></li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <nav class="responsive-tab">
                            <ul>
                                <li class="uk-active"><a class="active" href="#">Timeline</a></li>
                                <li><a href="timeline-friends.html">About</a></li>
                                <li><a href="timeline-friends.html">Friend</a></li>
                                <li><a href="timeline-friends.html">Photoes</a></li>
                                <li><a href="timeline-friends.html">Videos</a></li>
                            </ul>
                        </nav>
                        <div class="uk-visible@s">
                            <a href="#" class="nav-btn-more"> More</a>
                            <div uk-dropdown="pos: bottom-left ; mode:click">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">Moves</a></li>
                                    <li><a href="#">Likes</a></li>
                                    <li><a href="#">Events</a></li>
                                    <li><a href="#">Groups</a></li>
                                    <li><a href="#">Gallery</a></li>
                                    <li><a href="#">Sports</a></li>
                                    <li><a href="#">Gallery</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-small">
                <h2 class="my-lg-3"> Friends ( 2390 )</h2>
                <div class="uk-child-width-1-6@m uk-child-width-1-4@s uk-grid-row-small" uk-grid>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-1.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Dennis Han </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Stella Johnson </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Erica Jones </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-4.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Jonathan Ali </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-5.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Alex Dolgove </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-6.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Sarah Farah </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-7.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Erica Jones </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-1.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Dennis Han </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Stella Johnson </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Erica Jones </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-4.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Jonathan Ali </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media"> 
                                    <img src="assets/images/avatars/avatar-6.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Sarah Farah </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-7.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Erica Jones </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-5.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Alex Dolgove </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-1.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Dennis Han </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Stella Johnson </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Erica Jones </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="timeline.html">
                            <div class="user-data animate-this">
                                <div class="user-data-media">
                                    <img src="assets/images/avatars/avatar-4.jpg" alt="">
                                </div>
                                <div class="user-data-content">
                                    <h4> Jonathan Ali </h4>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="uk-flex uk-flex-center mt-4 mt-lg-8">
                    <a href="#" class="button btn-default small px-11 circle"> Veiw more </a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush