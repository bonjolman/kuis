@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'book')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Books </h1>
            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-1 mb-5">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggestions </a></li>
                        <li><a href="#"> Newest </a></li>
                        <li><a href="#"> My Books </a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle"> <i class="uil-plus"> </i> Create new </a>
            </div>

            <div class="uk-position-relative" uk-slider="finite: true">
                <div class="uk-slider-container pb-3">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s  pr-lg-1 uk-grid" uk-scrollspy="target: > div; cls: uk-animation-slide-bottom-small; delay: 100">
                        <li>
                            <a href="book-description.html" class="uk-text-bold">
                                <img src="assets/images/book/html5.jpg" class="mb-2 w-100 shadow rounded">
                                HTML5 Brick Breaker
                            </a>
                        </li>
                        <li>
                            <a href="book-description.html" class="uk-text-bold">
                                <img src="assets/images/book/css3.jpg" class="mb-2 w-100 shadow rounded">
                                CSS3 Web Development
                            </a>
                        </li>
                        <li>
                            <a href="book-description.html" class="uk-text-bold">
                                <img src="assets/images/book/vue-2-basics-.jpg" class="mb-2 w-100 shadow rounded">
                                Vue.js 2 Basics
                            </a>
                        </li>
                        <li>
                            <a href="book-description.html" class="uk-text-bold">
                                <img src="assets/images/book/php.jpg" class="mb-2 w-100 shadow rounded">
                                PHP for Beginners
                            </a>
                        </li>
                        <li>
                            <a href="book-description.html" class="uk-text-bold">
                                <img src="assets/images/book/win8.jpg" class="mb-2 w-100 shadow rounded">
                                WIN8 App Development
                            </a>
                        </li>
                        <li>
                            <a href="book-description.html" class="uk-text-bold">
                                <img src="assets/images/book/html5.jpg" class="mb-2 w-100 shadow rounded">
                                HTML5 Brick Breaker
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <h2 class="mt-4"> Category</h2>
            <nav class="responsive-tab">
                <ul>
                    <li class="uk-active"><a href="#">JavaScript</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">Coding</a></li>
                </ul>
            </nav>

            <div class="section-small">
                <div class="uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2" uk-grid>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/html5.jpg" class="mb-2 w-100 shadow rounded">
                            HTML5 Brick Breaker
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/css3.jpg" class="mb-2 w-100 shadow rounded">
                            CSS3 Web Development
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/vue-2-basics-.jpg" class="mb-2 w-100 shadow rounded">
                            Vue.js 2 Basics
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/php.jpg" class="mb-2 w-100 shadow rounded">
                            PHP for Beginners
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/win8.jpg" class="mb-2 w-100 shadow rounded">
                            WIN8 App Development
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/win8.jpg" class="mb-2 w-100 shadow rounded">
                            WIN8 App Development
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/vue-2-basics-.jpg" class="mb-2 w-100 shadow rounded">
                            Vue.js 2 Basics
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/php.jpg" class="mb-2 w-100 shadow rounded">
                            PHP for Beginners
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/html5.jpg" class="mb-2 w-100 shadow rounded">
                            HTML5 Brick Breaker
                        </a>
                    </div>
                    <div>
                        <a href="book-description.html" class="uk-text-bold">
                            <img src="assets/images/book/css3.jpg" class="mb-2 w-100 shadow rounded">
                            CSS3 Web Development
                        </a>
                    </div>
                </div>
            </div>

            <!-- pagination-->
            <ul class="uk-pagination uk-flex-center uk-margin-medium">
                <li class="uk-active">
                    <span>1</span>
                </li>
                <li>
                    <a href="#">2</a>
                </li>
                <li>
                    <a href="#">3</a>
                </li>
                <li>
                    <a href="#">4</a>
                </li>
                <li>
                    <a href="#">5</a>
                </li>
                <li>
                    <a href="#"><i class="icon-feather-chevron-right uk-margin-small-top"></i></a>
                </li>
                <li>
                    <a href="#">12</a>
                </li>
            </ul>
        </div>
    </div>

@endsection

@push('after-script')

@endpush