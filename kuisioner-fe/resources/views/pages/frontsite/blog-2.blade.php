@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog-2')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Blog </h1>
            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-1 mb-5">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggestions </a></li>
                        <li><a href="#"> Newest </a></li>
                        <li><a href="#"> My videos </a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle uk-visible@s"> 
                    <i class="uil-plus"></i> Create new
                </a>
            </div>

            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-expand@m">
                    <!--blog 1-->
                    <div class="blog-article">
                        <a href="blog-single.html">
                            <h2>10 Interesting JavaScript and CSS Libraries for November 2020 </h2>
                        </a>
                        <p class="blog-articl-meta">
                            <strong> Bootstrap </strong>
                            <a href="user-profile.html"> Nataly Birch </a>
                            •
                            <span> 24 October </span>
                        </p>
                        <a href="blog-single.html">
                            <div class="blog-article-thumbnail">
                                <div class="blog-article-thumbnail-inner">
                                    <img src="assets/images/blog/img-3.jpg" alt="">
                                </div>
                            </div>
                        </a>
                        <p class="blog-article-content">Nam liber tempor cum soluta nobis eleifend option congue
                            nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum mayi dolor
                            sit amet, conse ctetuer adipiscing elit, nibh euismod tincidunt ut laoreet
                            <a href="blog-single.html"> Read more ..</a>
                        </p>
                    </div>

                    <!-- blog 2 -->
                    <div class="blog-article">
                        <a href="blog-single.html">
                            <h2>Learn New 10 Interesting JavaScript and CSS libraries for 2020 </h2>
                        </a>
                        <p class="blog-articl-meta">
                            <strong> Bootstrap </strong>
                            <a href="user-profile.html"> Nataly Birch </a>
                            •
                            <span> 24 October </span>
                        </p>
                        <a href="blog-single.html">
                            <div class="blog-article-thumbnail">
                                <div class="blog-article-thumbnail-inner">
                                    <img src="assets/images/blog/img-4.jpg" alt="">
                                </div>
                            </div>
                        </a>
                        <p class="blog-article-content">Nam liber tempor cum soluta nobis eleifend option congue
                            nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum mayi dolor
                            sit amet, conse ctetuer adipiscing elit, nibh euismod tincidunt ut laoreet
                            <a href="blog-single.html"> Read more ..</a>
                        </p>
                    </div>

                    <!-- blog 3 -->
                    <div class="blog-article">
                        <a href="blog-single.html">
                            <h2> 10 amazing web demos and experiments </h2>
                        </a>
                        <p class="blog-articl-meta">
                            <strong> Software </strong>
                            <a href="user-profile.html"> Nataly Birch </a>
                            •
                            <span> 24 October </span>
                        </p>
                        <a href="blog-single.html">
                            <div class="blog-article-thumbnail">
                                <div class="blog-article-thumbnail-inner">
                                    <img src="assets/images/blog/img-1.jpg" alt="">
                                </div>
                            </div>
                        </a>
                        <p class="blog-article-content">Nam liber tempor cum soluta nobis eleifend option congue
                            nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum mayi dolor
                            sit amet, conse ctetuer adipiscing elit, nibh euismod tincidunt ut laoreet
                            <a href="blog-single.html"> Read more ..</a>
                        </p>
                    </div>

                    <!-- blog 4 -->
                    <div class="blog-article">
                        <a href="blog-single.html">
                            <h2> Learn 10 Awesome Web Dev Tools and Resources For 2020 </h2>
                        </a>
                        <p class="blog-articl-meta">
                            <strong> Bootstrap </strong>
                            <a href="user-profile.html"> Nataly Birch </a>
                            •
                            <span> 24 October </span>
                        </p>
                        <a href="blog-single.html">
                            <div class="blog-article-thumbnail">
                                <div class="blog-article-thumbnail-inner">
                                    <img src="assets/images/blog/img-4.jpg" alt="">
                                </div>
                            </div>
                        </a>
                        <p class="blog-article-content">Nam liber tempor cum soluta nobis eleifend option congue
                            nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum mayi dolor
                            sit amet, conse ctetuer adipiscing elit, nibh euismod tincidunt ut laoreet
                            <a href="blog-single.html"> Read more ..</a>
                        </p>
                    </div>

                    <!-- pagination menu -->
                    <ul class="uk-pagination my-5 uk-flex-center" uk-margin>
                        <li><a href="#"><span uk-pagination-previous></span></a></li>
                        <li class="uk-disabled"><span>...</span></li>
                        <li><a href="#">4</a></li>
                        <li class="uk-active"><span>7</span></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">10</a></li>
                        <li class="uk-disabled"><span>...</span></li>
                        <li><a href="#"><span uk-pagination-next></span></a></li>
                    </ul>
                </div>

                <div class="uk-width-1-3@s">
                    <div uk-sticky="offset:86;media: @m ; bottom:true">
                        <div class="uk-card-default rounded mb-4">
                            <ul class="uk-child-width-expand uk-tab" uk-switcher="animation: uk-animation-fade">
                                <li><a href="#">Newest</a></li>
                                <li><a href="#">Popular</a></li>
                            </ul>

                            <ul class="uk-switcher">
                                <!-- tab 1 -->
                                <li>
                                    <div class="py-3 px-4">
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Overview of SQL Commands and PDO </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img1.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Writing a Simple MVC App in Plain </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img2.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> How to Create and Use Bash Scripts </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img3.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <!-- tab 2 -->
                                <li>
                                    <div class="py-3 px-4">
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Overview of SQL Commands and PDO </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img1.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Writing a Simple MVC App in Plain </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img2.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> How to Create and Use Bash Scripts </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img3.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="uk-card-default rounded uk-overflow-hidden">
                            <div class="p-4 text-center">
                                <h4 class="uk-text-bold"> Subsicribe </h4>
                                <p> Get the Latest Posts and Article for us On Your Email</p>

                                <form class="mt-3">
                                    <input type="text" class="uk-input uk-form-small"
                                        placeholder="Enter your email address">
                                    <input type="submit" value="Subscirbe" class="button button-default block mt-3">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush