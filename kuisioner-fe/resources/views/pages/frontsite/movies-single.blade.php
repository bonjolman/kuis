@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <div class="main-moviewatch">
                <img src="assets/images/movies/mov-banner-2.jpg" alt="">
                <div class="main-moviewatch-content uk-light">
                    <h1> GUNGLE CRUISE </h1>
                    <p><i class="icon-material-outline-star text-yellow"></i> 4.5  · 73 min · 2009</p>
                </div>
            </div>

            <div class="moviecontent">
                <div class="moviecontent-infos">
                    <div class="mve_thmb"> 
                        <img src="assets/images/movies/mov-1.jpg" alt=""> 
                    </div>
                    <div class="mve_nfo">
                        <p class="about-film">Uniformed New Orleans PD officer Danny Fisher earns his promotion to detective to his near-rather lucky, albeit brave, arrest of ruthless terrorist arms-dealer Miles Jackson, whose girlfriend Erica Kessen got accidentally killed during the dirty FBI-operation. Exactly a year later, Miles has escaped and blows up Danny's house as foretaste of a Herculean race to accomplish twelve near-impossible tasks against the clock, otherwise his kidnapped wife Molly will be killed. Yet at the end, another master-plan is suspected.</p>                                
                        <ul>
                            <li>
                                <p>
                                    <strong>Stars:</strong>
                                    <span>Jimmy Heuang , Ashley Scott</span>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <strong>Genre:</strong>
                                    <span>ACTION</span>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <strong>Producer:</strong>
                                    <span> Renny Harltn</span>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <strong>Views:</span>
                                    <span>13873</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="embed-video my-5">
                    <iframe src="https://www.youtube.com/embed/XXcAL1pTHzQ" frameborder="0" uk-video="automute: true" allowfullscreen uk-responsive></iframe>
                </div>

                <div class="comments mt-lg-6 mt-3">
                    <h3>Comments (5201)</h3>
                    <ul>
                        <li>
                            <div class="comments-avatar">
                                <img src="assets/images//avatars/avatar-1.jpg" alt="">
                            </div>
                            <div class="comment-content">
                                <div class="comment-by">
                                    Jonathan Madano <span>Student</span>
                                    <a href="#" class="reply">
                                        <i class="icon-line-awesome-undo"></i>
                                        Reply
                                    </a>
                                </div>
                                <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                    nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
                                    enim ad
                                    minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                                    nisl ut
                                    aliquip ex ea commodo consequat. Nam liber tempor cum soluta nobis
                                    eleifend
                                    option
                                    congue 
                                </p>
                            </div>

                            <ul>
                                <li>
                                    <div class="comments-avatar">
                                        <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-by">
                                            Stella Johnson<span>Student</span>
                                            <a href="#" class="reply">
                                                <i class="icon-line-awesome-undo"></i>
                                                Reply
                                            </a>
                                        </div>
                                        <p> Nam liber tempor cum soluta nobis eleifend option congue ut
                                            laoreet
                                            dolore
                                            magna aliquam erat volutpat nihil imperdiet doming id quod mazim
                                            placerat
                                            facer possim assum. Lorem ipsum dolor sit amet
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="comments-avatar">
                                        <img src="assets/images/avatars/avatar-3.jpg" alt="">
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-by">
                                            Adrian Mohani <span>Student</span>
                                            <a href="#" class="reply">
                                                <i class="icon-line-awesome-undo"></i>
                                                Reply
                                            </a>
                                        </div>
                                        <p>tempor cum soluta nobis eleifend option congue ut laoreet dolore
                                            magna
                                            aliquam erat volutpat.
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <div class="comments-avatar">
                                <img src="assets/images/avatars/avatar-4.jpg" alt="">
                            </div>
                            <div class="comment-content">
                                <div class="comment-by">
                                    Alex Dolgove<span>Student</span>
                                    <a href="#" class="reply">
                                        <i class="icon-line-awesome-undo"></i>
                                        Reply
                                    </a>
                                </div>
                                <p>Nam liber tempor cum soluta nobis eleifend option congue ut laoreet
                                    dolore magna
                                    aliquam erat volutpat nihil imperdiet doming id quod mazim placerat
                                    facer possim
                                    assum. Lorem ipsum dolor sit amet
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>

                <hr>
                <div class="comments">
                    <h3>Add Comment </h3>
                    <ul>
                        <li>
                            <div class="comment-content p-0">
                                <form class="uk-grid-small uk-grid" uk-grid="">
                                    <div class="uk-width-1-2@s uk-first-column">
                                        <label class="uk-form-label">Name</label>
                                        <input class="uk-input" type="text" placeholder="Name">
                                    </div>
                                    <div class="uk-width-1-2@s">
                                        <label class="uk-form-label">Email</label>
                                        <input class="uk-input" type="text" placeholder="Email">
                                    </div>
                                    <div class="uk-width-1-1@s uk-grid-margin uk-first-column">
                                        <label class="uk-form-label">Comment</label>
                                        <textarea class="uk-textarea" placeholder="Enter Your Comments her..." style=" height:160px"></textarea>
                                    </div>
                                    <div class="uk-grid-margin uk-first-column">
                                        <input type="submit" value="submit" class="button btn-schoolmedia">
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
                
            <div class="section-header mt-lg-4">
                <div class="section-header-left">
                    <h3> Related Movies </h3>
                </div>
            </div>

            <div class="section-small pt-0">
                <div uk-slider="finite: true">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-grid mb-3">
                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named
                                                Frank 
                                            </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-2.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Mulan </h2>
                                        <h6>Juiced By Jimmy Heuang </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per
                                                family
                                            </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-5.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Sefirin Kizi</h2>
                                        <h6>Arab </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named
                                                Frank 
                                            </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-3.jpg" alt="">
                                    </div>
                                    <div class="mov-card-details ">
                                        <h2> Skyfall </h2>
                                        <h6> United Kindom </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Fantasy</span>
                                            <span class="scifi">Sci Fi</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per
                                                family
                                            </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-4.jpg" alt="">
                                    </div>

                                    <div class="mov-card-details">
                                        <h2> Code8 </h2>
                                        <h6> Juiced By Jimmy Heuang </h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Romantic</span>
                                            <span class="scifi">Action</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>When the Emperor of China issues a decree that one man per
                                                family
                                            </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="movies-single.html">
                                <div class="mov-card">
                                    <div class="poster">
                                        <img src="assets/images/movies/mov-1.jpg">
                                    </div>
                                    <div class="mov-card-details">
                                        <h2>Jungle Cruise</h2>
                                        <h6>United States</h6>

                                        <div class="mov-card-rating">
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <i class="icon-material-outline-star"></i>
                                            <span>5/5</span>
                                        </div>

                                        <div class="mov-card-tags">
                                            <span class="fantasy">Action</span>
                                            <span class="scifi">Dramma</span>
                                        </div>

                                        <div class="mov-card-info">
                                            <p>Set during the early 20th century, a riverboat captain named
                                                Frank 
                                            </p>
                                        </div>

                                        <div class="mov-card-act">
                                            <h4>Cast</h4>
                                            <ul>
                                                <li> <img src="assets/images/movies/cast-1.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-2.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-3.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-4.jpg" alt=""></li>
                                                <li> <img src="assets/images/movies/cast-5.jpg" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush