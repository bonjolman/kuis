@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'Kuisioner')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <table class="uk-table uk-table-hover uk-table-striped uk-table-divider">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Institusi</th>
                        <th>JU</th>
                        <th>Pemilik</th>
                        <th>Sifat</th>
                        <th>Bentuk</th>
                        <th>Nilai</th>
                        <th>Poin</th>
                        <th>PU</th>
                        <th>PD</th>
                        <th>SP</th>
                        <th>Isi</th>
                        <th>Chat</th>
                        <th>Kirim</th>
                        <th>Scoin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        
    </div>

@endsection

@push('after-script')

@endpush