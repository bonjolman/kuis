@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'Kuisioner')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <table class="uk-table uk-table-hover uk-table-striped uk-table-divider">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Kuisioner - Ku</th>
                        <th>Sifat</th>
                        <th>Institusi</th>
                        <th>NSPN</th>
                        <th>JU</th>
                        <th>Nama Partisipan</th>
                        <th>Scoin</th>
                        <th>PP</th>
                        <th>PR</th>
                        <th>SR</th>
                        <th>Hasil</th>
                        <th>Bayar</th>
                        <th>Komentar Pelimik</th>
                        <th>Komentar Responden</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        
    </div>

@endsection

@push('after-script')

@endpush