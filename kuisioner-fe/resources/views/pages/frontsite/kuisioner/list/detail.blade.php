@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'Kuisioner')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <table class="uk-table uk-table-hover uk-table-striped uk-table-divider">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Kuisioner</th>
                        <th>Bentuk</th>
                        <th>Sifat</th>
                        <th>Pand</th>
                        <th>Kuis</th>
                        <th>Nilai</th>
                        <th>Bursa</th>
                        <th>No</th>
                        <th>Res</th>
                        <th>Isi</th>
                        <th>Hasil</th>
                        <th>Edit</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        
    </div>

@endsection

@push('after-script')

@endpush