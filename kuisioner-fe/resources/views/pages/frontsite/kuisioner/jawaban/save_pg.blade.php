@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'Kuisioner - Tambah Instrumen Pilihan Ganda')

@section('content')

<style>

.check-off {
    color:white;
    background-color:white;
    border-color:white;
    border:1px solid white;
}

.check-on {
    color:green;
    background-color:white;
    border-color:green;
    border:1px solid;
}

</style>

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <form>
                <div class="uk-grid-small" uk-grid>
                <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">ID</label>
                        <select class="uk-select">
                            <option>K-999999</option>
                            <option>Option 02</option>
                        </select>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Sifat</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='' >
                        </div>
                    </div>
                </div>  

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Nama Kuisioner</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='' >
                        </div>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Bentuk</label>
                        <div class="uk-inline" style='width:100%'> 
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='Pilihan Ganda'>
                        </div>
                    </div>
                </div>  

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Keterangan Kuisioner</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <textarea class="uk-textarea" style="resize: none;" rows="5" placeholder="Textarea" disabled></textarea>
                        </div>
                        
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Panduan</label>
                        
                        <div class="uk-inline">
                            <span class="uk-form-icon" uk-icon="icon: cloud-download" style='color:white;margin-left:10px;'></span>
                            <button type="submit" class="uk-button uk-button-primary" style='padding-left:60px;'>Download Panduan</button>
                        </div>

                    </div>
                </div>           
            </form>
        </div>

        <div class="main_content_inner">
            <table class="uk-table">
                <caption></caption>
                <thead>
                    <tr>
                        <th style='border:1px solid;text-align:center;width:5%' >No</th>
                        <th style='border:1px solid;text-align:center;width:20%' >Soal</th>
                        <th style='border:1px solid;text-align:center;width:45%' colspan=2>Jawaban</th>
                        <th style='border:1px solid;text-align:center;width:5%' >Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style='border:1px solid;' rowspan=5>1.</td>
                        <td style='border:1px solid;' rowspan=5>Many of Changer other live in the world</td>
                        <td style='width:5%;text-align:center;'>
                            A
                        </td>
                        <td style='border-left:1px solid;'>
                            SDS
                        </td>
                        <td style='border-left:1px solid;border-right:1px solid;'>
                            <a href="" class="uk-icon-button check-off" style='' uk-icon="check"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:center;'>
                            B
                        </td>
                        <td style='border-left:1px solid;'>
                            FF
                        </td>
                        <td style='border-left:1px solid;border-right:1px solid;'>
                            <a href="" class="uk-icon-button check-off" style='' uk-icon="check"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:center;'>
                            C
                        </td>
                        <td style='border-left:1px solid;'>
                            DD
                        </td>
                        <td style='border-left:1px solid;border-right:1px solid;'>
                            <a href="" class="uk-icon-button check-off" style='' uk-icon="check"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:center;'>
                            D
                        </td>
                        <td style='border-left:1px solid;'>
                            GG
                        </td>
                        <td style='border-left:1px solid;border-right:1px solid;'>
                            <a href="" class="uk-icon-button check-off" style='' uk-icon="check"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style='border-bottom:1px solid;text-align:center;'>
                            E
                        </td>
                        <td style='border-left:1px solid;border-bottom:1px solid;'>
                            ZZ
                        </td>
                        <td style='border-left:1px solid;border-right:1px solid;border-bottom:1px solid;'>
                            <a href="" class="uk-icon-button check-off" style='' uk-icon="check" idx=''></a>
                        </td>
                    </tr>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>

@endsection

@push('after-script')

<script>
    $('.check-off').on('click',function(){
        $('.check-on').addClass('check-off');
        $('.check-on').removeClass('check-on');
        
        $(this).removeClass('check-off');
        $(this).addClass('check-on');
        return false;
    });
</script>

@endpush