@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'Kuisioner - Tambah Instrumen Upload')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <form>
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">ID</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='' >
                        </div>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Sifat</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='' >
                        </div>
                    </div>
                    
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">&nbsp;</label>
                        
                        <div class="uk-inline">
                            <span class="uk-form-icon" uk-icon="icon: cloud-upload" style='color:white;margin-left:10px;'></span>
                            <button type="submit" class="uk-button uk-button-primary" style='padding-left:60px;'>Upload Instrumen</button>
                        </div>

                    </div>
                </div>  

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Nama Kuisioner</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='' >
                        </div>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Nilai Scoin</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='' >
                        </div>
                    </div>
                </div>  

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Keterangan Kuisioner</label>
                        <div class="uk-inline" style='width:100%'>
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <textarea class="uk-textarea" style="resize: none;" rows="5" placeholder="Textarea" disabled></textarea>
                        </div>
                        
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Bentuk</label>
                        <div class="uk-inline" style='width:100%'> 
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text" disabled value='Pilihan Ganda'>
                        </div>
                    </div>
                </div>       
            </form>
        </div>

        

@endsection

@push('after-script')

@endpush