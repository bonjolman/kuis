@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'Kuisioner')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <form>
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text" uk-disabled>Waktu Buat</label>
                        <div class="uk-inline">
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: lock"></a>
                            <input class="uk-input"  type="text">
                        </div>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Bentuk</label>
                        <select class="uk-select">
                            <option>Option 01</option>
                            <option>Option 02</option>
                        </select>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">&nbsp;</label>
                        
                        <div class="uk-inline">
                            <span class="uk-form-icon" uk-icon="icon: plus"></span>
                            <button type="submit" class="uk-button uk-button-primary">Buat Kuisioner</button>
                        </div>

                    </div>
                </div>  

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">ID Kuisioner</label>
                        <div class="uk-inline">
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: user"></a>
                            <input class="uk-input"  type="text">
                        </div>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Sifat</label>
                        <select class="uk-select">
                            <option>Option 01</option>
                            <option>Option 02</option>
                        </select>
                    </div>
                    <div class="uk-width-1-3@s">
                    </div>
                </div>   

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Nama Kuisioner</label>
                        <input class="uk-input" type="text" placeholder="50">
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Nilai Score</label>
                        <input class="uk-input" type="text" placeholder="50">
                    </div>
                    <div class="uk-width-1-3@s">
                    </div>
                </div> 

                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Keterangan Kuisioner</label>
                        <textarea class="uk-textarea" rows="5" placeholder="Textarea"></textarea>
                    </div>
                    <div class="uk-width-1-3@s">
                        <label class="uk-form-label" for="form-stacked-text">Point</label>
                        <input class="uk-input" type="text" placeholder="50">
                    </div>
                    <div class="uk-width-1-3@s">
                    </div>
                </div>       
            </form>
        </div>

        <div class="main_content_inner">
            <form>
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-4@s">
                        <div uk-form-custom="target: true">
                            <input type="file">
                            <span class="uk-form-icon" uk-icon="icon: cloud-upload"></span>
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Upload File" disabled>
                        </div>
                    </div>
                    <div class="uk-width-1-5@s">
                        <div class="uk-inline">
                            <span class="uk-form-icon" uk-icon="icon: plus"></span>
                            <button type="submit" class="uk-button uk-button-primary">Buat Panduan</button>
                        </div>
                    </div>
                    
                </div>         
            </form>
        </div>
    </div>

@endsection

@push('after-script')

@endpush