@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'course intro')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="course-intro-banner">
            <img src="assets/images/avatars/profile-cover.jpg" class="course-intro-banner-img" alt="">
            <div class="course-intro-banner-info uk-light main_content_inner" style="max-width: 1160px">
                <h6> Web Design for Beginners </h6>
                <h1 class="mb-lg-6"> Build Responsive Real World Websites
                <hr class="uk-visible@m"> with HTML5 and CSS3 </h1>
                <p><a href="#course-intro" class="uk-link-reset" uk-scroll> View Course details </a></p>

                <div class="course-details-info">
                    <ul>
                        <li> <i class="icon-feather-sliders"></i> Advance level </li>
                        <li> By <a href="user-profile-1.html">Jonathan Madano </a></li>
                        <li>
                            <div class="star-rating">
                                <span class="avg"> 4.8 </span> 
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                                <span class="star"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="main_content_inner" style="max-width: 1160px">
            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-2-3@m">
                    <div class="course-description-content" id="course-intro">
                        <h3> Description</h3>
                        <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                            euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                            minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                            nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                            consequat
                        </p>
                        <p> consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                            dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                            exerci
                        </p>

                        <h3> What you’ll learn </h3>
                        <div class="uk-child-width-1-2@s uk-grid" uk-grid="">
                            <div class="uk-first-column">
                                <ul class="list-2">
                                    <li>Setting up the environment </li>
                                    <li>Advanced HTML Practices</li>
                                    <li>Build a portfolio website</li>
                                    <li>Responsive Designs</li>
                                </ul>
                            </div>
                            <div>
                                <ul class="list-2">
                                    <li>Understand HTML Programming</li>
                                    <li>Code HTML</li>
                                    <li>Start building beautiful websites</li>
                                </ul>
                            </div>
                        </div>

                        <h3> Requirements </h3>
                        <ul class="list-1">
                            <li>Any computer will work: Windows, macOS or Linux</li>
                            <li>Basic programming HTML and CSS.</li>
                            <li>Basic/Minimal understanding of JavaScript</li>
                        </ul>
                    </div>

                    <h2 class="uk-heading-line mt-lg-5"><span> curriculum</span></h2>
                    <ul class="course-curriculum uk-accordion" uk-accordion="multiple: true">
                        <li class="uk-open">
                            <a class="uk-accordion-title" href="#"> Html Introduction </a>
                            <div class="uk-accordion-content" aria-hidden="false">
                                <!-- course-video-list -->
                                <ul class="course-curriculum-list">
                                    <li> What is HTML <span> 23 min </span></li>
                                    <li> What is a Web page? <span> 23 min </span> </li>
                                    <li> Your First Web Page 
                                        <a href="#trailer-modal" uk-toggle=""> Preview </a> <span> 23 min </span>
                                    </li>
                                    <li> Brain Streak <span> 23 min </span> </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="uk-accordion-title" href="#"> Your First webpage</a>
                            <div class="uk-accordion-content" hidden="" aria-hidden="true">
                                <!-- course-video-list -->
                                <ul class="course-curriculum-list">
                                    <li> Headings <span> 23 min </span></li>
                                    <li> Paragraphs <span> 23 min </span></li>
                                    <li> Emphasis and Strong Tag 
                                        <a href="#trailer-modal" uk-toggle=""> Preview </a> <span> 23 min </span>
                                    </li>
                                    <li> Brain Streak <span> 23 min </span></li>
                                    <li> Live Preview Feature <span> 23 min </span></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="uk-accordion-title" href="#"> Some Special Tags </a>
                            <div class="uk-accordion-content" hidden="" aria-hidden="true">
                                <!-- course-video-list -->
                                <ul class="course-curriculum-list">
                                    <li> The paragraph tag <span> 23 min </span></li>
                                    <li> The break tag 
                                        <a href="#trailer-modal" uk-toggle=""> Preview </a> <span> 23 min </span>
                                    </li>
                                    <li> Headings in HTML <span> 23 min </span></li>
                                    <li> Bold, Italics Underline <span> 23 min </span></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="uk-accordion-title" href="#"> HTML Advanced level </a>
                            <div class="uk-accordion-content" hidden="" aria-hidden="true">
                                <!-- course-video-list -->
                                <ul class="course-curriculum-list">
                                    <li> Something to Ponder<span> 23 min </span></li>
                                    <li> Tables <span> 23 min </span></li>
                                    <li> HTML Entities 
                                        <a href="#trailer-modal" uk-toggle=""> Preview </a><span> 23 min </span></li>
                                    <li> HTML Iframes <span> 23 min </span></li>
                                    <li> Some important things <span> 23 min </span></li>
                                </ul>
                            </div>
                        </li>
                    </ul>

                    <h2 class="uk-heading-line mt-lg-5"><span> Review </span></h2>
                    <div class="comments mt-4">
                        <ul>
                            <li>
                                <div class="comments-avatar">
                                    <img src="assets/images//avatars/avatar-1.jpg" alt="">
                                </div>
                                <div class="comment-content">
                                    <div class="comment-by">Jonathan Madano <span>Student</span>
                                        <div class="comment-stars">
                                            <div class="star-rating">
                                                <span class="star"></span>
                                                <span class="star"></span>
                                                <span class="star"></span>
                                                <span class="star"></span>
                                                <span class="star"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                                        nibh
                                        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
                                        enim ad
                                        minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl
                                        ut
                                        aliquip ex ea commodo consequat. Nam liber tempor cum soluta nobis eleifend
                                        option
                                        congue 
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="comments-avatar">
                                    <img src="assets/images/avatars/avatar-4.jpg" alt="">
                                </div>
                                <div class="comment-content">
                                    <div class="comment-by">Alex Dolgove<span>Student</span>
                                        <div class="comment-stars">
                                            <div class="star-rating">
                                                <span class="star"></span>
                                                <span class="star"></span>
                                                <span class="star"></span>
                                                <span class="star"></span>
                                                <span class="star"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Nam liber tempor cum soluta nobis eleifend option congue ut laoreet dolore
                                        magna
                                        aliquam erat volutpat nihil imperdiet doming id quod mazim placerat facer
                                        possim
                                        assum. Lorem ipsum dolor sit amet
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <hr>
                    <div class="comments">
                        <h3>Add Review </h3>
                        <ul>
                            <li>
                                <div class="comments-avatar">
                                    <img src="../assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="comment-content">
                                    <form class="uk-grid-small uk-grid" uk-grid="">
                                        <div class="uk-width-1-2@s uk-first-column">
                                            <label class="uk-form-label">Name</label>
                                            <input class="uk-input" type="text" placeholder="Name">
                                        </div>
                                        <div class="uk-width-1-2@s">
                                            <label class="uk-form-label">Email</label>
                                            <input class="uk-input" type="text" placeholder="Email">
                                        </div>
                                        <div class="uk-width-1-1@s uk-grid-margin uk-first-column">
                                            <label class="uk-form-label">Comment</label>
                                            <textarea class="uk-textarea" placeholder="Enter Your Comments her..." style=" height:160px"></textarea>
                                        </div>
                                        <div class="uk-grid-margin uk-first-column">
                                            <input type="submit" value="submit" class="button btn-schoolmedia">
                                        </div>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="uk-width-1-3@m uk-flex-last@s uk-flex-first">
                    <div class="course-intro-card" uk-sticky="offset:95px;bottom:true;animation: uk-animation-slide-top-medium; bottom ; media: @s">
                        <div class="uk-inline overly-gradient-top">
                            <img src="assets/images/course/1.png" alt="">
                            <div class="uk-overlay uk-light uk-position-center">
                                <a href="#"> <img src="assets/images/icon-play.svg" width="80" alt=""> </a>
                            </div>
                        </div>
                        <div class="course-intro-card-innr">
                            <p class="mb-2"> 
                                <span class="uk-h1 uk-text-bold"> $12.99 </span>
                                <s class="uk-margin-small-left"> $12.99 </s>
                                <s class="uk-margin-small-left"> $19.99 </s> 
                            </p>
                            <p> ! Hour Left This price</p>

                            <div class="uk-child-width-1-2 uk-grid-small mb-5" uk-grid>
                                <div>
                                    <a class="button btn-default uk-width-1-1" href="#"> Start </a>
                                </div>
                                <div>
                                    <button class="button btn-schoolmedia uk-width-1-1"> Add wishlist </button>
                                </div>
                            </div>

                            <h4 class=""> This Course Include </h4>
                            <div class="uk-child-width-1-2 uk-grid-small uk-text-small" uk-grid>
                                <div>
                                    <span> <i class="uil-video"></i> 28 hours video</span>
                                </div>
                                <div>
                                    <span> <i class="uil-file-alt"></i> 12 Article </span>
                                </div>
                                <div>
                                    <span> <i class="uil-graduation-hat"></i> Certificate </span>
                                </div>
                                <div>
                                    <span> <i class="uil-lock"></i> lifetime access </span>
                                </div>
                                <div class="uk-width-1-1">
                                    <span> <i class="uil-lock"></i> Full lifetime access </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush