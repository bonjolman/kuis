@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Pages </h1>
            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-1 mb-5">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggestions </a></li>
                        <li><a href="#"> Newest </a></li>
                        <li><a href="#"> My pages </a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle uk-visible@s"> 
                    <i class="uil-plus"> </i> Create new
                </a>
            </div>

            <!-- slider -->
            <div class="uk-position-relative" uk-slider="finite: true">
                <div class="uk-slider-container pb-3">
                    <ul class="uk-slider-items uk-child-width-1-3@m uk-child-width-1-2@s  pr-lg-1 uk-grid" uk-scrollspy="target: > div; cls: uk-animation-slide-bottom-small; delay: 100">
                        <li>
                            <div class="px-sm-2" uk-grid>
                                <div class="uk-width-expand  uk-flex uk-flex-middle">
                                    <img src="assets/images/brand/brand-avatar-3.png" class="rounded-sm mr-2" width="40" alt="">
                                    <h4 class="mb-0"> Sky Accounting </h4>
                                </div>
                                <div class="uk-width-auto"> 
                                    <a href="group-feed.html" class="button btn-schoolmedia small circle"> 
                                        <i class="uil-thumbs-up"></i> Like
                                    </a>
                                </div>
                                <div class="uk-width-1-1 mt-2">
                                    <p class="mb-0"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                        diam nonummy nibh euismod erat volutpat...
                                    </p>
                                    <div class="uk-flex uk-flex-middle mt-2">
                                        <div class="avatar-group uk-width-auto">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>Stella</strong> and 2 freind are Liked </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="px-sm-2" uk-grid>
                                <div class="uk-width-expand  uk-flex uk-flex-middle">
                                    <img src="assets/images/brand/brand-avatar-2.png" class="rounded-sm mr-2" width="40" alt="">
                                    <h4 class="mb-0"> Reveal Store</h4>
                                </div>
                                <div class="uk-width-auto"> 
                                    <a href="group-feed.html" class="button btn-schoolmedia small circle"> 
                                        <i class="uil-thumbs-up"></i> Like
                                    </a>
                                </div>
                                <div class="uk-width-1-1 mt-2">
                                    <p class="mb-0"> Ut wisi enim ad minim veniam, quis nostrud exerci tation
                                        ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo..
                                    </p>
                                    <div class="uk-flex uk-flex-middle mt-2">
                                        <div class="avatar-group uk-width-auto">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>Stella</strong> and 2 freind are Liked </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="px-sm-2" uk-grid>
                                <div class="uk-width-expand  uk-flex uk-flex-middle"> 
                                    <img src="assets/images/brand/brand-avatar-4.png" class="rounded-sm mr-2" width="40" alt="">
                                    <h4 class="mb-0"> Phase designers</h4>
                                </div>
                                <div class="uk-width-auto"> 
                                    <a href="group-feed.html" class="button btn-schoolmedia small circle"> 
                                        <i class="uil-thumbs-up"></i> Like
                                    </a>
                                </div>
                                <div class="uk-width-1-1 mt-2">
                                    <p class="mb-0"> Ut wisi enim ad minim veniam, quis nostrud exerci tation
                                        ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. ..
                                    </p>
                                    <div class="uk-flex uk-flex-middle mt-2">
                                        <div class="avatar-group uk-width-auto">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>Stella</strong> and 2 freind are Liked </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="px-sm-2" uk-grid>
                                <div class="uk-width-expand  uk-flex uk-flex-middle"> 
                                    <img src="assets/images/brand/brand-avatar-3.png" class="rounded-sm mr-2" width="40" alt="">
                                    <h4 class="mb-0"> Suranna Media</h4>
                                </div>
                                <div class="uk-width-auto"> 
                                    <a href="group-feed.html" class="button btn-schoolmedia small circle"> 
                                        <i class="uil-thumbs-up"></i> Like
                                    </a>
                                </div>
                                <div class="uk-width-1-1 mt-2">
                                    <p class="mb-0"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                        diam nonummy nibh euismod erat volutpat.....
                                    </p>
                                    <div class="uk-flex uk-flex-middle mt-2">
                                        <div class="avatar-group uk-width-auto">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>Stella</strong> and 2 freind are Liked </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <div class="section-small">
                <h3> You Liked </h3>
                <nav class="responsive-tab style-4 mb-4" uk-sticky="offset:60;cls-active:bg-white">
                    <ul>
                        <li class="uk-active"><a href="#"> You liked <span>120</span> </a></li>
                        <li><a href="#"> Recently </a></li>
                        <li><a href="#"> My pages </a></li>
                    </ul>
                </nav>

                <div class="uk-child-width-1-2@m uk-grid-collapse" uk-grid>
                    <div>
                        <div class="pages-card">
                            <div class="page-card-media"> 
                                <img src="assets/images/brand/brand-avatar-4.png" alt="">
                            </div>
                            <div class="page-card-innr">
                                <h3> Phase Designers <span> Design</span> </h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed quis nostrud exerci
                                    diam nonummy nibh euismod erat volutpat..
                                </p>

                                <div class="uk-flex uk-flex-middle mt-3">
                                    <div class="avatar-group uk-width-auto">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-5.jpg" class="avatar avatar-xs rounded-circle">
                                    </div>
                                    <div class="uk-width-expand pl-2">
                                        <p> <strong>Richard</strong> and 2 freind are Liked </p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-card-btn">
                                <a href="group-feed.html" class="button btn-soft-schoolmedia"> 
                                    <i class="uil-thumbs-up"></i>
                                    UnLike
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="pages-card">
                            <div class="page-card-media"> 
                                <img src="assets/images/brand/brand-avatar-3.png" alt="">
                            </div>
                            <div class="page-card-innr">
                                <h3> Sky Accounting <span> Technolgy</span> </h3>
                                <p> ullamcorper suscipit lobortis nisl wisi enimad minim veniam, quis nostrud exerci
                                    tation ut aliquip ex ea commodo....
                                </p>

                                <div class="uk-flex uk-flex-middle mt-3">
                                    <div class="avatar-group uk-width-auto">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                    </div>
                                    <div class="uk-width-expand pl-2">
                                        <p> <strong>John</strong> and 6 freind are Liked </p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-card-btn">
                                <a href="group-feed.html" class="button btn-soft-schoolmedia"> 
                                    <i class="uil-thumbs-up"></i>
                                    UnLike
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="pages-card">
                            <div class="page-card-media"> 
                                <img src="assets/images/brand/brand-avatar-2.png" alt="">
                            </div>
                            <div class="page-card-innr">
                                <h3> Reveal Store <span>shopping</span> </h3>
                                <p> ullamcorper suscipit lobortis quis nostrud exerci tation ullamcorper suscipit
                                    lobortis nisl ut aliquip ex ea commodo ...
                                </p>

                                <div class="uk-flex uk-flex-middle mt-3">
                                    <div class="avatar-group uk-width-auto">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-6.jpg" class="avatar avatar-xs rounded-circle">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-5.jpg" class="avatar avatar-xs rounded-circle">
                                    </div>
                                    <div class="uk-width-expand pl-2">
                                        <p> <strong>Alex</strong> and 7 freind are Liked </p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-card-btn">
                                <a href="group-feed.html" class="button btn-soft-schoolmedia"> 
                                    <i class="uil-thumbs-up"></i>
                                    UnLike
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="pages-card">
                            <div class="page-card-media"> 
                                <img src="assets/images/brand/brand-avatar-3.png" alt="">
                            </div>
                            <div class="page-card-innr">
                                <h3> Suranna brand <span>shooping</span></h3>
                                <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed quis nostrud
                                    exerci diam nonummy nibh euismod erat volutpat..
                                </p>

                                <div class="uk-flex uk-flex-middle mt-3">
                                    <div class="avatar-group uk-width-auto">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-3.jpg" class="avatar avatar-xs rounded-circle">
                                    </div>
                                    <div class="uk-width-expand pl-2">
                                        <p> <strong>Stella</strong> and 42 freind are Liked </p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-card-btn">
                                <a href="group-feed.html" class="button btn-soft-schoolmedia"> 
                                    <i class="uil-thumbs-up"></i>
                                    UnLike
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="pages-card">
                            <div class="page-card-media"> 
                                <img src="assets/images/brand/brand-avatar-3.png" alt="">
                            </div>
                            <div class="page-card-innr">
                                <h3> Sky Accounting <span> Technolgy</span> </h3>
                                <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed quis nostrud
                                    exerci diam nonummy nibh euismod erat volutpat..
                                </p>
                                <div class="uk-flex uk-flex-middle mt-3">
                                    <div class="avatar-group uk-width-auto">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                    </div>
                                    <div class="uk-width-expand pl-2">
                                        <p> <strong>John</strong> and 6 freind are Liked </p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-card-btn">
                                <a href="group-feed.html" class="button btn-soft-schoolmedia">
                                    <i class="uil-thumbs-up"></i>
                                    UnLike
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="pages-card">
                            <div class="page-card-media">
                                <img src="assets/images/brand/brand-avatar-4.png" alt="">
                            </div>
                            <div class="page-card-innr">
                                <h3> Phase Designers <span> Design</span> </h3>
                                <p> ullamcorper suscipit lobortis quis nostrud exerci tation ullamcorper suscipit
                                    lobortis nisl ut aliquip ex ea commodo.....
                                </p>

                                <div class="uk-flex uk-flex-middle mt-3">
                                    <div class="avatar-group uk-width-auto">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                        <img alt="Image placeholder" src="assets/images/avatars/avatar-5.jpg" class="avatar avatar-xs rounded-circle">
                                    </div>
                                    <div class="uk-width-expand pl-2">
                                        <p> <strong>Richard</strong> and 2 freind are Liked </p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-card-btn">
                                <a href="group-feed.html" class="button btn-soft-schoolmedia"> 
                                    <i class="uil-thumbs-up"></i>
                                    UnLike
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- pagination menu -->
                <ul class="uk-pagination my-5 uk-flex-center" uk-margin>
                    <li><a href="#">1</a></li>
                    <li class="uk-active"><span>7</span></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">10</a></li>
                    <li class="uk-disabled"><span>...</span></li>
                    <li><a href="#"><span uk-pagination-next></span></a></li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush