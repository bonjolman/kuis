@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Blog </h1>
            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-1 mb-5">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggestions </a></li>
                        <li><a href="#"> Newest </a></li>
                        <li><a href="#"> My videos </a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle uk-visible@s"> 
                    <i class="uil-plus"></i> Create new
                </a>
            </div>

            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-expand">
                    <!-- Blog Post -->
                    <a href="blog-single.html" class="blog-post">
                        <!-- Blog Post Thumbnail -->
                        <div class="blog-post-thumbnail">
                            <div class="blog-post-thumbnail-inner">
                                <span class="blog-item-tag">Tips</span>
                                <img src="assets/images/blog/img-1.jpg" alt="">
                            </div>
                        </div>
                        <!-- Blog Post Content -->
                        <div class="blog-post-content">
                            <div class="blog-post-content-info">
                                <span class="blog-post-info-tag button btn-soft-red"> Softwares </span>
                                <span class="blog-post-info-date">10 June</span>
                            </div>
                            <h3>10 amazing web demos and experiments </h3>
                            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim...
                            </p>
                        </div>
                    </a>

                    <!-- Blog Post -->
                    <a href="blog-single.html" class="blog-post">
                        <!-- Blog Post Thumbnail -->
                        <div class="blog-post-thumbnail">
                            <div class="blog-post-thumbnail-inner">
                                <span class="blog-item-tag">Tools</span>
                                <img src="assets/images/blog/img-2.jpg" alt="">
                            </div>
                        </div>
                        <!-- Blog Post Content -->
                        <div class="blog-post-content">
                            <div class="blog-post-content-info">
                                <span class="blog-post-info-tag button btn-soft-schoolmedia"> Softwares </span>
                                <span class="blog-post-info-date">10 June</span>
                            </div>
                            <h3>10 Awesome Web Dev Tools and Resources For 2020</h3>
                            <p>Nam liber tempor cum soluta nobis nihil imperdiet doming id tempor cum soluta nobis
                                quod mazim placerat facer possim soluta..
                            </p>
                        </div>
                    </a>

                    <!-- Blog Post -->
                    <a href="blog-single.html" class="blog-post">
                        <!-- Blog Post Thumbnail -->
                        <div class="blog-post-thumbnail">
                            <div class="blog-post-thumbnail-inner">
                                <img src="assets/images/blog/img-3.jpg" alt="">
                            </div>
                        </div>
                        <!-- Blog Post Content -->
                        <div class="blog-post-content">
                            <div class="blog-post-content-info">
                                <span class="blog-post-info-tag button btn-soft-yellow"> Softwares </span>
                                <span class="blog-post-info-date">10 June</span>
                            </div>
                            <h3>10 Interesting JavaScript and CSS Libraries </h3>
                            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim...
                            </p>
                        </div>
                    </a>

                    <!-- Blog Post -->
                    <a href="blog-single.html" class="blog-post">
                        <!-- Blog Post Thumbnail -->
                        <div class="blog-post-thumbnail">
                            <div class="blog-post-thumbnail-inner">
                                <img src="assets/images/blog/img-4.jpg" alt="">
                            </div>
                        </div>
                        <!-- Blog Post Content -->
                        <div class="blog-post-content">
                            <div class="blog-post-content-info">
                                <span class="blog-post-info-tag button btn-soft-schoolmedia"> Programming </span>
                                <span class="blog-post-info-date">10 June</span>
                            </div>
                            <h3>10 Interesting JavaScript and CSS libraries for 2020 </h3>
                            <p>Nam liber tempor cum soluta nobis nihil imperdiet doming id tempor cum soluta nobis
                                quod mazim placerat facer possim soluta..
                            </p>
                        </div>
                    </a>
                </div>
                <div class="uk-width-1-3@s">
                    <div uk-sticky="offset:86;media: @m ; bottom:true">
                        <div class="uk-card-default rounded mb-4">
                            <ul class="uk-child-width-expand uk-tab" uk-switcher="animation: uk-animation-fade">
                                <li><a href="#">Newest</a></li>
                                <li><a href="#">Popular</a></li>
                            </ul>

                            <ul class="uk-switcher">
                                <!-- tab 1 -->
                                <li>
                                    <div class="py-3 px-4">
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Overview of SQL Commands and PDO </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img1.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Writing a Simple MVC App in Plain </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img2.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> How to Create and Use Bash Scripts </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img3.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <!-- tab 2 -->
                                <li>
                                    <div class="py-3 px-4">
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Overview of SQL Commands and PDO </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img1.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> Writing a Simple MVC App in Plain </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img2.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                        <div class="uk-grid-small" uk-grid>
                                            <div class="uk-width-expand">
                                                <p> How to Create and Use Bash Scripts </p>
                                            </div>
                                            <div class="uk-width-1-3">
                                                <img src="assets/images/category/img3.jpg" alt="" class="rounded-sm">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="uk-card-default rounded uk-overflow-hidden">
                            <div class="p-4 text-center">
                                <h4 class="uk-text-bold"> Subsicribe </h4>
                                <p> Get the Latest Posts and Article for us On Your Email</p>

                                <form class="mt-3">
                                    <input type="text" class="uk-input uk-form-small"
                                        placeholder="Enter your email address">
                                    <input type="submit" value="Subscirbe" class="button button-default block mt-3">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush