@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <h1> Questions </h1>
            <nav class="responsive-tab style-1 mb-5">
                <ul>
                    <li class="uk-active"><a href="#"> Suggestions </a></li>
                    <li><a href="#"> Newest </a></li>
                    <li><a href="#"> My Quiz </a></li>
                </ul>
            </nav>

            <div class="uk-grid-large" uk-grid>
                <div class="uk-width-2-3@s">
                    <!-- quiz 1 -->
                    <div class="quiz-block">
                        <div class="quiz-block-content">
                            <h3> What can I learn right now in 10 minutes that will be useful for the rest of my life? </h3>

                            <div class="user-details-card pt-0  uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Stella Johnson <span class="ml-0"> June 2nd, 2019 </span>
                                </div>
                            </div>

                            <p> Lorem ipsum dolor sit cum soluta nobis eleifend option congue nihil imperdiet doming
                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                                volutpat 
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>
                        </div>

                        <!-- quiz block replay -->
                        <div class="quiz-block-replay">
                            <h3 class="uk-heading-bullet mb-4"> Answars (5) </h3>
                            <div class="user-details-card pt-0 pb-2 uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-1.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Alex Dolgove <span class="ml-0"> June 4nd, 2020 </span>
                                </div>
                            </div>

                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>

                            <hr class="my-3">

                            <div class="user-details-card pt-0 pb-2 uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-6.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Sara montaga <span class="ml-0"> June 2nd, 2019 </span>
                                </div>
                            </div>

                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Answers <span> 210</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares"> 
                                        <i class="uil-refresh"></i> Shares <span> 120</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- quiz group slider -->
                    <div class="section-small">
                        <div class="uk-position-relative pb-3" uk-slider>
                            <div class="uk-slider-container py-3">
                                <ul class="uk-slider-items uk-grid">
                                    <!-- slider 1 -->
                                    <li class="uk-width-2-5">
                                        <div class="quiz-group-card">
                                            <!-- Group Card Thumbnail -->
                                            <div class="quiz-group-card-thumbnail">
                                                <img src="assets/images/group/group-cover-1.jpg" alt="">
                                            </div>

                                            <!-- Group Card Content -->
                                            <div class="quiz-group-card-content">
                                                <img src="assets/images/group/group-5.jpg" alt="">
                                                <h3> Investment Advice </h3>
                                                <p class="info"> Life stories from people around the world. </p>

                                                <div class="quiz-group-card-btns">
                                                    <a href="#" class="button btn-default small"> 
                                                        <i class="uil-plus-circle"></i> Follow 1.3K 
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <!-- slider 2 -->
                                    <li class="uk-width-2-5">
                                        <div class="quiz-group-card">
                                            <!-- Group Card Thumbnail -->
                                            <div class="quiz-group-card-thumbnail">
                                                <img src="assets/images/group/group-cover-4.jpg" alt="">
                                            </div>

                                            <!-- Group Card Content -->
                                            <div class="quiz-group-card-content">
                                                <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                                <h3> Tech World </h3>
                                                <p class="info"> Learn more about technology, insurance and education. </p>

                                                <div class="quiz-group-card-btns">
                                                    <a href="#" class="button btn-default small"> 
                                                        <i class="uil-plus-circle"></i> Follow 2.3K 
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <!-- slider 3 -->
                                    <li class="uk-width-2-5">
                                        <div class="quiz-group-card">
                                            <!-- Group Card Thumbnail -->
                                            <div class="quiz-group-card-thumbnail">
                                                <img src="assets/images/group/group-cover-3.jpg" alt="">
                                            </div>

                                            <!-- Group Card Content -->
                                            <div class="quiz-group-card-content">
                                                <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                                <h3> Essay Writings </h3>
                                                <p class="info"> Lots of essay for kids and school students. </p>

                                                <div class="quiz-group-card-btns">
                                                    <a href="#" class="button btn-default small"> 
                                                        <i class="uil-plus-circle"></i> Follow 3.1K 
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                                <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                            </div>
                        </div>
                    </div>

                    <!-- quiz two -->
                    <div class="quiz-block">
                        <div class="quiz-block-content">
                            <h3> What useful things should I learn? </h3>

                            <div class="user-details-card pt-0  uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-7.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Adrian Mohani <span class="ml-0"> June 2nd, 2019 </span>
                                </div>
                            </div>

                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis 
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                    <div class="mt-0 p-2" uk-dropdown="pos: bottom-right;mode:hover">
                                        <ul class="uk-nav uk-dropdown-nav">
                                            <li><a href="#"> Share </a></li>
                                            <li><a href="#"> Edit Quiz </a></li>
                                            <li><a href="#"> Disable Replays </a></li>
                                            <li><a href="#"> Add favorites </a></li>
                                            <li><a href="#" class="text-red"> Delete </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- quiz block comment -->
                        <div class="quiz-block-add-replay">
                            <img src="assets/images/avatars/avatar-7.jpg" alt="">
                            <input type="text" class="uk-input" placeholder="Add your Comment">
                            <button class="button btn-schoolmedia circle">Add comment</button>
                        </div>

                        <!-- quiz block replay -->
                        <div class="quiz-block-replay">
                            <h3 class="uk-heading-bullet mb-4"> Answars (1) </h3>
                            <div class="user-details-card pt-0 pb-2   uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Stella Johnson <span class="ml-0"> June 2nd, 2019 </span>
                                </div>
                            </div>

                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis 
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- quiz two -->
                    <div class="quiz-block">
                        <div class="quiz-block-content">
                            <h3> How can I learn any skills for freelancing from home? </h3>

                            <div class="user-details-card pt-0  uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-7.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Adrian Mohani <span class="ml-0"> June 2nd, 2019 </span>
                                </div>
                            </div>

                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>
                        </div>

                        <!-- quiz block replay -->
                        <div class="quiz-block-add-replay">
                            <img src="assets/images/avatars/avatar-7.jpg" alt="">
                            <input type="text" class="uk-input" placeholder="Add your replay">
                            <button class="button btn-schoolmedia circle">Add replay</button>
                        </div>

                        <!-- quiz block replay -->
                        <div class="quiz-block-replay">
                            <h3 class="uk-heading-bullet mb-4"> Answars (2) </h3>

                            <div class="user-details-card pt-0 pb-2   uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Stella Johnson <span class="ml-0"> June 2nd, 2019 </span>
                                </div>
                            </div>
                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis 
                            </p>
                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>

                            <hr class="my-3">

                            <!-- replay two -->
                            <div class="user-details-card pt-0 pb-2   uk-flex-top">
                                <div class="user-details-card-avatar">
                                    <img src="assets/images/avatars/avatar-5.jpg" alt="">
                                </div>
                                <div class="user-details-card-name">
                                    Alex Dolgove <span class="ml-0"> May 4nd, 2019 </span>
                                </div>
                            </div>

                            <p> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id
                                quod mazim placerat facer possim tempor cum soluta nobis 
                            </p>

                            <!-- quiz block buttons -->
                            <div class="btn-acts">
                                <div>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Answers">
                                        <i class="uil-comment-message"></i> Replays <span> 21</span>
                                    </a>
                                    <a href="#" class="button btn-white circle" uk-tooltip="Shares">
                                        <i class="uil-refresh"></i> Shares <span> 34</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="#"><i class="uil-share-alt"></i></a>
                                    <a href="#" class="#"><i class="uil-ellipsis-h"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- sidebar -->
                <div class="uk-width-expand">
                    <div uk-sticky="offset:90; bottom:true">
                        <h2> Recent Questions </h3>

                        <ul class="uk-list uk-list-divider">
                            <li>
                                <a href="#"> 
                                    What can I learn right now in 10 minutes that will be useful for the rest of my life? 
                                </a>
                            </li>
                            <li>
                                <a href="#"> How can I learn any skills for freelancing from home?</a>
                            </li>
                            <li>
                                <a href="#"> What can I learn in 10 minutes that will be useful for the rest of my life? </a>
                            </li>
                            <li>
                                <a href="#"> What useful things should I learn? </a>
                            </li>
                            <li>
                                <a href="#"> How can you learn faster? </a>
                            </li>
                            <li>
                                <a href="#"> What can I learn in one minute? </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush