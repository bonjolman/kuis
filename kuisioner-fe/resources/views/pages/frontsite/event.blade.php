@extends('layouts.frontsite')

{{-- set title --}}
@section('title', 'blog single')

@section('content')

    <!-- contents -->
    <div class="main_content">
        <div class="main_content_inner">
            <div id="spinneroverlay"></div>

            <h1> Events </h1>
            <div class="uk-flex uk-flex-between">
                <nav class="responsive-tab style-1">
                    <ul>
                        <li class="uk-active"><a href="#"> Suggested events </a></li>
                        <li><a href="#"> My events</a></li>
                        <li><a href="#"> Joined events</a></li>
                    </ul>
                </nav>
                <a href="#" class="button btn-schoolmedia small circle uk-visible@s"> 
                    <i class="uil-plus"> </i> Create new
                </a>
            </div>

            <div class="uk-position-relative" uk-slider="finite: true">
                <div class="uk-slider-container px-lg-1 py-3">
                    <ul class="uk-slider-items uk-child-width-1-4@m uk-child-width-1-3@s uk-grid-small uk-grid">
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-4.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 12</h3>
                                        <p> Jun</p>
                                    </div>

                                    <h4 class="events-list-name">
                                        <a href="#"> The global creative </a>
                                    </h4>

                                    <p>Uk brands</p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>12 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand">
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-3.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 20</h3>
                                        <p> May</p>
                                    </div>

                                    <h4 class="events-list-name">
                                        <a href="#"> Safety and Flight </a>
                                    </h4>

                                    <p> Upcomming </p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-3.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>24 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-2.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 10</h3>
                                        <p> Jully</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> Perspective is everything</a>
                                    </h4>

                                    <p> Feeding </p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-3.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-5.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>10 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand">
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-1.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 15</h3>
                                        <p> March</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> Accept Who I Am</a>
                                    </h4>

                                    <p> Conference </p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-6.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>12 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-4.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 12</h3>
                                        <p> Jun</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> The global creative </a>
                                    </h4>

                                    <p>Uk brands</p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>12 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <div class="section-header pb-0 mt-3">
                <div class="section-header-left">
                    <h3> Lists You may like </h3>
                </div>
                <div class="section-header-right">
                    <a href="#" class="see-all"> See all</a>
                </div>
            </div>

            <div class="uk-position-relative" uk-slider="finite: true">
                <div class="uk-slider-container py-3">
                    <ul class="uk-slider-items uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2 uk-grid-small uk-grid">
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-1.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> Miami </p>
                                        <h4> Hotels </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-2.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> Florida</p>
                                        <h4> Hotels </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-6.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> London</p>
                                        <h4> Cold Drink </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-3.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> Los Angeles </p>
                                        <h4> Hotels </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-4.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> Dubai </p>
                                        <h4> Tom Restaurant </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-5.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> Florida</p>
                                        <h4> Food </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="event-catagroy-card animate-this" data-src="assets/images/events/listing-1.jpg" uk-img>
                                    <div class="event-catagroy-card-content">
                                        <p> Miami </p>
                                        <h4> Hotels </h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <a class="uk-position-center-left-out uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small uk-hidden-hover slidenav-next" href="#" uk-slider-item="next"></a>
                </div>
            </div>

            <hr class="my-3 my-sm-2">
            <div uk-slider="finite: true">
                <div class="grid-slider-header">
                    <div>
                        <h3> Upcomming Events </h3>
                    </div>
                    <div class="grid-slider-header-link">
                        <a href="browse-channals.html" class="button btn-white uk-visible@m"> View all </a>
                        <a href="#" class="slide-nav-prev" uk-slider-item="previous"></a>
                        <a href="#" class="slide-nav-next" uk-slider-item="next"></a>
                    </div>
                </div>

                <div class=" px-lg-1 ">
                    <ul class="uk-slider-items uk-child-width-1-4@m uk-child-width-1-3@s uk-grid-small uk-grid">
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-4.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 12</h3>
                                        <p> Jun</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> The global creative </a>
                                    </h4>

                                    <p>Uk brands</p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-2.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>12 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand">
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-3.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 20</h3>
                                        <p> May</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> Safety and Flight </a>
                                    </h4>

                                    <p> Upcomming </p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-3.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>24 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-2.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 10</h3>
                                        <p> Jully</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> Perspective is everything</a>
                                    </h4>

                                    <p> Feeding </p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-3.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-5.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>10 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-1.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 15</h3>
                                        <p> March</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> Accept Who I Am</a>
                                    </h4>

                                    <p> Conference </p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-6.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>12 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="events-list">
                                <a href="#" class="event-cover">
                                    <img src="assets/images/events/img-4.jpg" alt="">
                                </a>

                                <div class="event-info">
                                    <div class="event-info-date">
                                        <h3> 12</h3>
                                        <p> Jun</p>
                                    </div>
                                    <h4 class="events-list-name">
                                        <a href="#"> The global creative </a>
                                    </h4>

                                    <p>Uk brands</p>
                                    <div class="uk-flex uk-grid mb-2" uk-grid>
                                        <div class="avatar-group uk-width-auto pl-4">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-1.jpg" class="avatar avatar-xs rounded-circle">
                                            <img alt="Image placeholder" src="assets/images/avatars/avatar-4.jpg" class="avatar avatar-xs rounded-circle">
                                        </div>
                                        <div class="uk-width-expand pl-2">
                                            <p> <strong>12 other </strong> are Intersted</p>
                                        </div>
                                    </div>

                                    <div class="event-btns uk-flex">
                                        <a href="#" class="button btn-schoolmedia block small uk-width-expand"> 
                                            <i class="uil-star"></i> Joined 
                                        </a>
                                        <a href="#" class="button btn-default small uk-width-auto button-icon ml-1">
                                            <i class="uil-share-alt"></i> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-script')

@endpush