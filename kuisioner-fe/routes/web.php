<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontsite\PageController@index')->name('home');

Route::group(['prefix' => 'kuisioner'], function () {
    Route::get('', 'Frontsite\User\KuisionerController@index')->name('kuisioner');

    Route::get('/instrumen', 'Frontsite\User\InstrumenController@index')->name('instrumen');

    Route::get('/isi_jawaban', 'Frontsite\User\JawabanController@index')->name('jawaban');

    Route::get('/undang_user', 'Frontsite\User\UserController@index')->name('undang_user');
});





