<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KuisionerDetail extends Model
{
    //

    public $timestamps = false;

    protected $table = 't2_kuisioner_detail';

    protected $primaryKey = 'id_kuisioner';

    protected $fillable = ['id_kuisioner', 'nilai_scoin', 'nilai_point'];

}
