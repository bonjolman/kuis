<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InstrumenPG extends Model
{
    //


    protected $table = 't2_kuisioner_instrumen_pg';

    protected $primaryKey = 'id_kuisioner';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['id_kuisioner', 'soal', 'jawaban', 'order_idx'];


}
