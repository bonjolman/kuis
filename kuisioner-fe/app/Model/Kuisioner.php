<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use App\Model\KuisionerDetail;

class Kuisioner extends Model
{
    //

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 't1_m_kuisioner';

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = ['id', 'name', 'id_sifat', 'id_bentuk','keterangan'];

    public function detail()
    {
        return $this->hasMany(KuisionerDetail::class,'id_kuisioner','id');
    }
}
