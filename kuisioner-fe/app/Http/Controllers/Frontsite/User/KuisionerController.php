<?php

namespace App\Http\Controllers\Frontsite\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Kuisioner;
use App\Model\KuisionerDetail;

class KuisionerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kuisioner::leftJoin('t2_kuisioner_detail', function($join) {
                $join->on('id', '=', 'id_kuisioner');
            })
            ->paginate(50);
        // return $data;
        return view('pages.frontsite.kuisioner.list.detail',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $date_create = Date('d/m/Y H:i:s');
        $code_create = 'K-001007';
        return view('pages.frontsite.kuisioner.create',['date_create' => $date_create, 'code_create' => $code_create]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $create = Kuisioner::create($request->all());
        if ($create)
        {
            $data = [
                'id_kuisioner' => $request->id,
                'nilai_scoin' => $request->score,
                'nilai_point' => $request->point,
            ];
            KuisionerDetail::create($data);
        }else{
            Kuisioner::delete($request->id);

            return redirect()->to('/kuisioner/create');
        }

        return redirect()->to('/kuisioner');
        echo "success";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
