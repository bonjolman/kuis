<?php

namespace App\Http\Controllers\Frontsite\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Kuisioner;
use App\Model\KuisionerDetail;
use App\Model\InstrumenPG;

class InstrumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id =  $request->all()['id'];

        $data = Kuisioner::leftJoin('t2_kuisioner_detail', function($join) {
            $join->on('id', '=', 'id_kuisioner');
        })->find($id);

        $bentuk = $data['id_bentuk'];

        $array_seq = array('A','B','C','D','E','F','G','H');

        if($bentuk == 1){
            $data_soal = InstrumenPG::where(['id_kuisioner'=>$id])->get();

            // return $data_soal;
            return view('pages.frontsite.kuisioner.instrumen.create_pg',['data'=>$data,'data_soal'=>$data_soal, 'array_seq'=> $array_seq]);
        }else if($bentuk == 2){
            return view('pages.frontsite.kuisioner.instrumen.create_sl',['data'=>$data]);
        }else if($bentuk == 3){
            return view('pages.frontsite.kuisioner.instrumen.create_upload',['data'=>$data]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('pages.frontsite.kuisioner.instrumen.create_pg');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $get = InstrumenPG::select('id')->where(['id_kuisioner'=>$request->id])->max('order_idx');

        $jumlah_soal = InstrumenPG::where(['id_kuisioner'=>$request->id])->get();

        $jawaban = '';
        for($i = 0; $i < count($request->jawaban);$i++){
            $jawaban .= $request->jawaban[$i];
            $jawaban .= ',';
        }
        $jawaban = substr($jawaban, 0, -1);

        $data = [
            'id_kuisioner' => $request->id,
            'soal' => $request->soal,
            'jawaban' => $jawaban,
            'order_idx' => $get === null ? 0 : $get + 1,
        ];

        // return $data;

        $ruls = ['A','B','C','D','E','F','G','H'];

        $create = InstrumenPG::create($data);
        if($create){
            $update = KuisionerDetail::where(['id_kuisioner'=>$request->id])->update(['jumlah_pilihan_pg'=>$request->jumlah_pilihan_jawaban,'jumlah_soal'=>count($jumlah_soal) + 1]);
        }else{
            $update = KuisionerDetail::where(['id_kuisioner'=>$request->id])->update(['jumlah_pilihan_pg'=>null]);
        }

        return redirect()->to('/kuisioner/instrumen?id='.$request->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
